#include <sstream>
#include <catch2/catch.hpp>
#include <ValueOf.hpp>
#include <TreeNodes.hpp>
#include <Lexer.hpp>
#include <Parser.hpp>
#include <Env.hpp>
#include <ThreeAddressCode.hpp>

TEST_CASE("ValueOf-simple", "[VALUEOF]")
{
  auto test = [](std::string const& source, std::string const& res){
		Lexer lexer;
		Parser parser { lexer };
		tree::Node& n = const_cast<tree::Node&>(parser(source));
		Env env;

		tree::ThreeAddressCode tac { &env };
		n.visit(tac);
		
		tree::ValueOf value { &env };
		n.visit(value);
		
		REQUIRE(res == value());		
	      };
  
  test("12", "12");
  test("3+7", "10");
  test("5-9", "-4");
  test("6*7", "42");
  test("21/3", "7");
  test("1+2*3", "7");

  test("true", "1");
  test("false", "0");

  test("!true", "0");
  test("!false", "1");

  test("true && true", "1");
  test("false && true", "0");
  test("true && false", "0");
  test("false && false", "0");

  test("true || true", "1");
  test("false || true", "1");
  test("true || false", "1");
  test("false || false", "0");

  test("true && true", "1");
  test("false && true", "0");
  test("true && false", "0");
  test("false && false", "0");

  test("_assert 3 * 2 == 6", "1");
  test("_assert 3 * 4 == 6", "0");
  test("_dump 3", "3");
  test("_dump 3 * 6 + 4", "22");

  test("x : int = 4;x;", "4");
  test("x : int = 6 * 7;x;", "42");
  test("x : bool = false;x;", "0");
  test("x : bool =!(false && false);x;", "1");
  test("x : bool = true;x;", "1");
}


TEST_CASE("ValueOf-ident-bug", "[VALUEOF]")
{
  Lexer lexer;
  Parser parser { lexer };
  tree::Node& n = const_cast<tree::Node&>(parser("x : int = 4; y : int = x * 2; y;"));

  Env env;
  
  tree::ThreeAddressCode tac { &env };
  n.visit(tac);
  
  tree::ValueOf value { &env };
  n.visit(value);
		
  REQUIRE("8" == value());
}

TEST_CASE("ValueOf-vardecl-bug", "[VALUEOF]")
{
  Lexer lexer;
  Parser parser { lexer };
  tree::Node& n = const_cast<tree::Node&>(parser("x : int = 326; x;"));

  Env env;

  REQUIRE( !env.contains("x") );
  tree::ThreeAddressCode tac { &env };
  n.visit(tac);

  REQUIRE( env.contains("x") );
  
  tree::ValueOf value { &env };
  n.visit(value);
		
  REQUIRE("326" == value());
}

TEST_CASE("ValueOf-vardecl-block-bug", "[VALUEOF]")
{
  Lexer lexer;
  Parser parser { lexer };
  tree::Node& n = const_cast<tree::Node&>(parser("{x : int = 326; x}"));

  Env env;
  tree::ValueOf value { &env };
  n.visit(value);
		
  REQUIRE("326" == value());
}

TEST_CASE("ValueOf-assign", "[VALUEOF]")
{
  Lexer lexer;
  Parser parser { lexer };
  tree::Node& n = const_cast<tree::Node&>(parser("x = 4"));

  Env env;
  tree::ValueOf value { &env };
  n.visit(value);
		
  REQUIRE("4" == value());
}

TEST_CASE("ValueOf-assign-declared", "[VALUEOF]")
{
  Lexer lexer;
  Parser parser { lexer };
  tree::Node& n = const_cast<tree::Node&>(parser("x : int = 4; x = 12"));

  Env env;
  tree::ValueOf value { &env };
  n.visit(value);
		
  REQUIRE("12" == value());
}

TEST_CASE("ValueOf-assign-in-block", "[VALUEOF]")
{
  Lexer lexer;
  Parser parser { lexer };
  tree::Node& n = const_cast<tree::Node&>(parser("{x : int = 4; x = 12; x}"));

  Env env;
  tree::ValueOf value { &env };
  n.visit(value);
		
  REQUIRE("12" == value());
}

TEST_CASE("ValueOf-if", "[VALUEOF]")
{
  SECTION("first-cond")
    {
      std::stringstream source;
      source << "if {" << std::endl;
      source << "(1 == 1) 1" << std::endl;
      source << "(2 == 2) 2" << std::endl;
      source << "(else) 3" << std::endl;
      source << "}" << std::endl;

      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>( parser(source.str()) );

      Env ev;
      tree::ValueOf value { &ev };  
      n.visit(value);
  
      REQUIRE("1" == value());
    }

  SECTION("second-cond")
    {
      std::stringstream source;
      source << "if {" << std::endl;
      source << "(1 == 2) 1" << std::endl;
      source << "(2 == 2) 2" << std::endl;
      source << "(else) 3" << std::endl;
      source << "}" << std::endl;

      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>( parser(source.str()) );

      Env ev;
      tree::ValueOf value { &ev };  
      n.visit(value);
  
      REQUIRE("2" == value());
    }

  SECTION("else-cond")
    {
      std::stringstream source;
      source << "if {" << std::endl;
      source << "(4 == 1) 1" << std::endl;
      source << "(2 == 1) 2" << std::endl;
      source << "(else) 3" << std::endl;
      source << "}" << std::endl;

      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>( parser(source.str()) );

      Env ev;
      tree::ValueOf value { &ev };  
      n.visit(value);
  
      REQUIRE("3" == value());
    }

  SECTION("no-value-no-else")
    {
      std::stringstream source;
      source << "if {" << std::endl;
      source << "(1 == 2) 1" << std::endl;
      source << "(2 == 1) 2" << std::endl;
      source << "}" << std::endl;

      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>( parser(source.str()) );

      Env ev;
      tree::ValueOf value { &ev };  
      n.visit(value);
  
      REQUIRE("0" == value());
    }
}

TEST_CASE("ValueOf-loop", "[VALUEOF]")
{
  Lexer lexer;
  Parser parser { lexer };
  Env env;
  tree::ThreeAddressCode tac { &env };
  std::stringstream source;
  source << "x : int = 0; y : int = 0;" << std::endl;
  source << "loop x < 10 {" << std::endl;
  source << "loop y < 10 {" << std::endl;
  source << "y = y + 1 {" << std::endl;
  source << "}" << std::endl;
  source << "x = x + 1 {" << std::endl;
  source << "}" << std::endl;
  tree::Node& node = const_cast<tree::Node&>( parser(source.str()) );
  node.visit(tac);
  tree::ValueOf vo { &env };
  node.visit(vo);
}
