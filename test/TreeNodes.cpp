#include <catch2/catch.hpp>
#include <TreeNodes.hpp> 

TEST_CASE("TreeNodes-compare", "[TREENODES]")
{
  SECTION("int")
    {
      REQUIRE( tree::Int(3) != tree::Int(4) );
      REQUIRE( tree::Int(4) == tree::Int(4) );
      REQUIRE( tree::Int(-9) == tree::Int(-9) );
      REQUIRE( tree::Int(7) != tree::Int(4) );
      REQUIRE( tree::Int(4) != tree::Int(7) );
      REQUIRE( tree::Int(-7) != tree::Int(-4) );
      REQUIRE( tree::Int(-4) != tree::Int(-7) );
    }

  SECTION("int-arith")
    {
      REQUIRE( tree::Add( new tree::Int(3), new tree::Int(9) ) == tree::Add( new tree::Int(3), new tree::Int(9) ));
      REQUIRE( tree::Add( new tree::Int(2), new tree::Int(9) ) != tree::Add( new tree::Int(3), new tree::Int(9) ));
      REQUIRE( tree::Add( new tree::Int(3), new tree::Int(9) ) != tree::Add( new tree::Int(3), new tree::Int(1) ));

      REQUIRE( tree::Sub( new tree::Int(3), new tree::Int(9) ) == tree::Sub( new tree::Int(3), new tree::Int(9) ));
      REQUIRE( tree::Sub( new tree::Int(2), new tree::Int(9) ) != tree::Sub( new tree::Int(3), new tree::Int(9) ));
      REQUIRE( tree::Sub( new tree::Int(3), new tree::Int(9) ) != tree::Sub( new tree::Int(3), new tree::Int(1) ));
      
      REQUIRE( tree::Mul( new tree::Int(3), new tree::Int(9) ) == tree::Mul( new tree::Int(3), new tree::Int(9) ));
      REQUIRE( tree::Mul( new tree::Int(2), new tree::Int(9) ) != tree::Mul( new tree::Int(3), new tree::Int(9) ));
      REQUIRE( tree::Mul( new tree::Int(3), new tree::Int(9) ) != tree::Mul( new tree::Int(3), new tree::Int(1) ));

      REQUIRE( tree::Div( new tree::Int(3), new tree::Int(9) ) == tree::Div( new tree::Int(3), new tree::Int(9) ));
      REQUIRE( tree::Div( new tree::Int(2), new tree::Int(9) ) != tree::Div( new tree::Int(3), new tree::Int(9) ));
      REQUIRE( tree::Div( new tree::Int(3), new tree::Int(9) ) != tree::Div( new tree::Int(3), new tree::Int(1) ));

      REQUIRE( tree::Mod( new tree::Int(7), new tree::Int(2) ) == tree::Mod( new tree::Int(7), new tree::Int(2) ));
    }

  SECTION("bool")
    {
      REQUIRE( tree::Bool(true) == tree::Bool(true) );
      REQUIRE( tree::Bool(false) == tree::Bool(false) );

      REQUIRE( tree::Bool(false) != tree::Bool(true) );
      REQUIRE( tree::Bool(true) != tree::Bool(false) );      
    }
    
  SECTION("bool-arith")
    {
      REQUIRE( tree::And( new tree::Bool(true), new tree::Bool(true)) ==
	       tree::And(  new tree::Bool(true), new tree::Bool(true)));
      
      REQUIRE( tree::And( new tree::Bool(false),new tree::Bool(true)) ==
	       tree::And( new tree::Bool(false), new tree::Bool(true)));
      
      REQUIRE( tree::And( new tree::Bool(false),new tree::Bool(true)) !=
	       tree::And( new tree::Bool(true), new tree::Bool(true)));
      
      REQUIRE( tree::And( new tree::Bool(true), new tree::Bool(false)) !=
	       tree::And( new tree::Bool(true), new tree::Bool(true)));
      
      REQUIRE( tree::And( new tree::Bool(true), new tree::Bool(true)) !=
	       tree::And( new tree::Bool(false), new tree::Bool(true)));
      
      REQUIRE( tree::And( new tree::Bool(true), new tree::Bool(true)) !=
	       tree::And( new tree::Bool(true), new tree::Bool(false)));

      REQUIRE( tree::Or( new tree::Bool(true), new tree::Bool(true)) ==
	       tree::Or( new tree::Bool(true), new tree::Bool(true)));
      
      REQUIRE( tree::Or( new tree::Bool(false), new tree::Bool(true)) ==
	       tree::Or( new tree::Bool(false), new tree::Bool(true)));
      
      REQUIRE( tree::Or( new tree::Bool(false), new tree::Bool(true)) !=
	       tree::Or( new tree::Bool(true),  new tree::Bool(true)));
      
      REQUIRE( tree::Or( new tree::Bool(true), new tree::Bool(false)) !=
	       tree::Or( new tree::Bool(true), new tree::Bool(true)));
      
      REQUIRE( tree::Or( new tree::Bool(true), new tree::Bool(true)) !=
	       tree::Or( new tree::Bool(false), new tree::Bool(true)));
      
      REQUIRE( tree::Or( new tree::Bool(true), new tree::Bool(true)) !=
	       tree::Or( new tree::Bool(true), new tree::Bool(false)));

      REQUIRE( tree::Not( new tree::Bool(true) ) ==
	       tree::Not( new tree::Bool(true) ) );
      
      REQUIRE( tree::Not( new tree::Bool(false) ) !=
	       tree::Not( new tree::Bool(true) ) );
    }

  SECTION("cmp")
    {
      REQUIRE( tree::Lt(new tree::Int(1), new tree::Int(2)) == tree::Lt(new tree::Int(1), new tree::Int(2)));
      REQUIRE( tree::Lt(new tree::Int(12), new tree::Int(2)) != tree::Lt(new tree::Int(1), new tree::Int(2)));

      REQUIRE( tree::Le(new tree::Int(1), new tree::Int(2)) == tree::Le(new tree::Int(1), new tree::Int(2)));
      REQUIRE( tree::Le(new tree::Int(12), new tree::Int(2)) != tree::Le(new tree::Int(1), new tree::Int(2)));

      REQUIRE( tree::Leq(new tree::Int(1), new tree::Int(2)) == tree::Leq(new tree::Int(1), new tree::Int(2)));
      REQUIRE( tree::Leq(new tree::Int(12), new tree::Int(2)) != tree::Leq(new tree::Int(1), new tree::Int(2)));

      REQUIRE( tree::Gt(new tree::Int(1), new tree::Int(2)) == tree::Gt(new tree::Int(1), new tree::Int(2)));
      REQUIRE( tree::Gt(new tree::Int(12), new tree::Int(2)) != tree::Gt(new tree::Int(1), new tree::Int(2)));

      REQUIRE( tree::Ge(new tree::Int(1), new tree::Int(2)) == tree::Ge(new tree::Int(1), new tree::Int(2)));
      REQUIRE( tree::Ge(new tree::Int(12), new tree::Int(2)) != tree::Ge(new tree::Int(1), new tree::Int(2)));

      REQUIRE( tree::Geq(new tree::Int(1), new tree::Int(2)) == tree::Geq(new tree::Int(1), new tree::Int(2)));
      REQUIRE( tree::Geq(new tree::Int(12), new tree::Int(2)) != tree::Geq(new tree::Int(1), new tree::Int(2)));

      REQUIRE( tree::Equal(new tree::Int(1), new tree::Int(2)) == tree::Equal(new tree::Int(1), new tree::Int(2)));
      REQUIRE( tree::Equal(new tree::Int(12), new tree::Int(2)) != tree::Equal(new tree::Int(1), new tree::Int(2)));

      REQUIRE( tree::Equiv(new tree::Int(1), new tree::Int(2)) == tree::Equiv(new tree::Int(1), new tree::Int(2)));
      REQUIRE( tree::Equiv(new tree::Int(12), new tree::Int(2)) != tree::Equiv(new tree::Int(1), new tree::Int(2)));
      
      REQUIRE( tree::Ne(new tree::Int(1), new tree::Int(2)) == tree::Ne(new tree::Int(1), new tree::Int(2)));
      REQUIRE( tree::Ne(new tree::Int(12), new tree::Int(2)) != tree::Ne(new tree::Int(1), new tree::Int(2)));

      REQUIRE( tree::Neq(new tree::Int(1), new tree::Int(2)) == tree::Neq(new tree::Int(1), new tree::Int(2)));
      REQUIRE( tree::Neq(new tree::Int(12), new tree::Int(2)) != tree::Neq(new tree::Int(1), new tree::Int(2)));      
    }

}
