#include <iostream>
#include <catch2/catch.hpp>
#include <MsgStatus.hpp>
#include <Lexer.hpp>
#include <Parser.hpp>
#include <TreeNodes.hpp>
#include <TypeController.hpp>
#include <Env.hpp>

TEST_CASE("MsgStatus-parser", "[MSGSTATUS]")
{
  Lexer lexer;
  Parser parser { lexer };

  MsgStatus::init();
  
  parser("_assert;");
  parser("_dump;");
  parser("2/;");
  parser("3*;");
  parser("2+3+;");
  parser("(1+2;");

  parser("&& true;");
  parser("false &&;");
  parser("|| true;");
  parser("false ||;");
  
  std::vector<std::string> errors = MsgStatus::errors_str();

  REQUIRE(19 == errors.size());
  REQUIRE("Unexpected token ; line 1" == errors[0]);  
  REQUIRE("Assert : missing argument" == errors[1]);
  REQUIRE("Unexpected token ; line 1" == errors[2]);
  REQUIRE("Dump : missing argument" == errors[3]);
  REQUIRE("Unexpected token ; line 1" == errors[4]);
  REQUIRE("Div : missing arguments" == errors[5]);
  REQUIRE("Unexpected token ; line 1" == errors[6]);
  REQUIRE("Mul : missing arguments" == errors[7]);
  REQUIRE("Unexpected token ; line 1" == errors[8]);
  REQUIRE("Add : missing arguments" == errors[9]);
  REQUIRE("ClosePar : missing close parenthesis" == errors[10]);
  REQUIRE("Unexpected token && line 1" == errors[11]);
  REQUIRE("And : missing arguments" == errors[12]);
  REQUIRE("Unexpected token ; line 1" == errors[13]);
  REQUIRE("And : missing arguments" == errors[14]);
  REQUIRE("Unexpected token || line 1" == errors[15]);
  REQUIRE("Or : missing arguments" == errors[16]);
  REQUIRE("Unexpected token ; line 1" == errors[17]);
  REQUIRE("Or : missing arguments" == errors[18]);
}

TEST_CASE("MsgStatus-type", "[MSGSTATUS]")
{
  auto test = [](std::string const& source, std::string const msg) {
		MsgStatus::init();
		Env env;
		tree::TypeController tc { &env };
		Lexer lexer;
		Parser parser { lexer };
		tree::Node& n = const_cast<tree::Node&>(parser(source));
		n.visit(tc);      
		std::vector<std::string> errors = MsgStatus::errors_str();
		REQUIRE( 1 == errors.size() );
		REQUIRE(msg == errors[0]);
	      };

  test("5 < true;", "Lt : expecting int, got bool");
  test("2 > false;", "Gt : expecting int, got bool");
  test("5 <= true;", "Le : expecting int, got bool");
  test("2 >= false;", "Ge : expecting int, got bool");
  test("5 <== true;", "Leq : expecting int, got bool");
  test("2 >== false;", "Geq : expecting int, got bool");
  test("true == 5;", "Equal : type mismatch");
  test("5 == true;", "Equal : type mismatch");
  test("false === 54;", "Equiv : type mismatch");
  test("54 === false;", "Equiv : type mismatch");
  test("true + 1;", "Add : expecting int, got bool");
  test("false * 2;", "Mul : expecting int, got bool");
  test("true / true;", "Div : expecting int, got bool");
  test("false % 23;", "Mod : expecting int, got bool");
  test("-true;", "Usub : expecting int, got bool");

  test("54 && true;", "And : expecting bool, got int");
  test("false || 7;", "Or : expecting bool, got int");
  test("!54;", "Not : expecting bool, got int");  
}

TEST_CASE("MsgStatus-no-error-int", "[MSGSTATUS]")
{
  Lexer lexer;
  Parser parser { lexer };
  MsgStatus::init();
  
  parser("3 * 5 + 7 - 6 / 2 % 4;");
  
  std::vector<std::string> errors = MsgStatus::errors_str();  
  REQUIRE( errors.empty() );
}

TEST_CASE("MsgStatus-no-error-bool", "[MSGSTATUS]")
{
  Lexer lexer;
  Parser parser { lexer };
  MsgStatus::init();
  
  parser("true && false || true && !false;");
  
  std::vector<std::string> errors = MsgStatus::errors_str();  
  REQUIRE( errors.empty() );
}
