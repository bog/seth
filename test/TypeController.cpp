#include <iostream>
#include <catch2/catch.hpp>
#include <TypeController.hpp>
#include <Parser.hpp>
#include <Lexer.hpp>
#include <Env.hpp>

TEST_CASE("TypeController-int-arith", "[TYPECONTROLLER]")
{
  SECTION("simple-int")
    {      
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("4;"));
      n.visit(tc);
      REQUIRE( !tc.error() );
    }

  SECTION("arith")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("4 + 1 * 2 / 3 % 4;"));
      n.visit(tc);
      REQUIRE( !tc.error() );
    }

  SECTION("err-add")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("4 + true;"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }

  SECTION("err-sub")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("false - 2;"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }

  SECTION("err-mul")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("-2 * true;"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }

  SECTION("err-div")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("false / true;"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }

  SECTION("err-mod")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("3%false; "));
      n.visit(tc);
      REQUIRE( tc.error() );
    }
}


TEST_CASE("TypeController-bool-arith", "[TYPECONTROLLER]")
{
  SECTION("simple-bool-true")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("true;"));
      n.visit(tc);
      REQUIRE( !tc.error() );
    }

  SECTION("simple-bool-false")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("false;"));
      n.visit(tc);
      REQUIRE( !tc.error() );
    }

  SECTION("arith")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("!(true && false || true);"));
      n.visit(tc);
      REQUIRE( !tc.error() );
    }

  SECTION("err-and")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("true && 7;"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }

  SECTION("err-or")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("45 || false;"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }

  SECTION("err-not")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("!27;"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }
}

TEST_CASE("TypeController-cmp", "[TYPECONTROLLER]")
{
  SECTION("simple-cmp")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("4 < 7;"));
      n.visit(tc);
      REQUIRE( !tc.error() );
    }

  SECTION("arith-and")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("3 > 7 && 5 == 1;"));
      n.visit(tc);
      REQUIRE( !tc.error() );
    }

  SECTION("arith-or")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("1 === 1 || false;"));
      n.visit(tc);
      REQUIRE( !tc.error() );
    }

  SECTION("arith-not")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("!(3 < 78 && 4 == 2);"));
      n.visit(tc);
      REQUIRE( !tc.error() );
    }
    
  SECTION("err-lt")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("4 < true;"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }

  SECTION("err-le")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("4 <= false;"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }

  SECTION("err-leq")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("true <== false;"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }

  ////
  SECTION("err-gt")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("4 > true;"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }

  SECTION("err-ge")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("false >= 5;"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }

  SECTION("err-geq")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("7 >== false;"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }    
}

TEST_CASE("TypeController-assert", "[TYPECONTROLLER]")
{
  SECTION("simple-assert-true")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("_assert true;"));
      n.visit(tc);
      REQUIRE( !tc.error() );
    }

  SECTION("simple-assert-false")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("_assert false;"));
      n.visit(tc);
      REQUIRE( !tc.error() );
    }

  SECTION("simple-assert-expr0")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("_assert 4 + 1 < 3;"));
      n.visit(tc);
      REQUIRE( !tc.error() );
    }

  SECTION("simple-assert-expr1")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("_assert 3 * 6 === 4;"));
      n.visit(tc);
      REQUIRE( !tc.error() );
    }

  SECTION("err-int0")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("_assert 4;"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }

  SECTION("err-int1")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("_assert 6*7;"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }
}

TEST_CASE("TypeController-vardecl", "[TYPECONTROLLER]")
{
  SECTION("int-ok")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("x : int = 42;"));
      n.visit(tc);
      REQUIRE( !tc.error() );
    }

  SECTION("bool-ok")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("y : bool = false;"));
      n.visit(tc);
      REQUIRE( !tc.error() );
    }


  SECTION("int-err0")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("x : bool = 42;"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }

  SECTION("int-err1")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("x : int = true;"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }

  SECTION("bool-err")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("y : bool = 0;"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }  
    
  SECTION("unknown-type-err")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("y : kazog = 0;"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }
}

TEST_CASE("TypeController-ident-bug", "[TYPECONTROLLER]")
{
  SECTION("int")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("x : int = 4; y : int = 6; z : int = x + y;"));
      n.visit(tc);

      REQUIRE( !tc.error() );
    }

    SECTION("bool")
    {
      MsgStatus::init();
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("x : bool = true; y : bool = true; z : bool = x && y;"));
      n.visit(tc);
      
      REQUIRE( !tc.error() );
    }

    SECTION("int-bool-err")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("x : int = 0; y : bool = true; z : bool = x && y;"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }

    SECTION("bool-int-err")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("x : bool = false; y : int = 42; z : int = x + y;"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }
}

TEST_CASE("TypeController-assign", "[TYPECONTROLLER]")
{
  SECTION("int")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("x : int = 4; x = true"));
      n.visit(tc);

      REQUIRE( tc.error() );
    }

  SECTION("bool")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("x : bool = true; x = 4"));
      n.visit(tc);

      REQUIRE( tc.error() );
    }
}

TEST_CASE("TypeController-if", "[TYPECONTROLLER]")
{
  SECTION("entry-ok")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("if { (4 < 12) 0 (5 == 5) 1}"));
      n.visit(tc);

      REQUIRE( !tc.error() );
    }

    SECTION("entry-no-bool-err-0")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("if { (4) 0 (5 == 5) 0}"));
      n.visit(tc);

      REQUIRE( tc.error() );
    }

    SECTION("entry-no-bool-err-1")
    {
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("if { (4 == 4) 0 (5) 0}"));
      n.visit(tc);

      REQUIRE( tc.error() );
    }
}

TEST_CASE("TypeController-array", "[TYPECONTROLLER]")
{
  SECTION("int-err")
    {      
      Env env;
      tree::TypeController tc { &env };
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>(parser("x : int = [1, 2, 3]"));
      n.visit(tc);
      REQUIRE( tc.error() );
    }
}
