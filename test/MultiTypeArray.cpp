#include <catch2/catch.hpp>
#include <MultiTypeArray.hpp>
#include <Type.hpp>

TEST_CASE("MultiTypeArray-push-back", "[MULTITYPEARRAY]")
{
  MultiTypeArray array;
  array.push_back(true);
  array.push_back(4);
  
  REQUIRE(true == array.get(0).value.b);  
  REQUIRE(4 == array.get(1).value.i32);
}

TEST_CASE("MultiTypeArray-nested-push-back", "[MULTITYPEARRAY]")
{
  MultiTypeArray array0;
  array0.push_back(true);
  array0.push_back(4);
  
  MultiTypeArray array1;
  array1.push_back(array0);
  array1.push_back(7);
  
  REQUIRE(&array0 == array1.get(0).array);
  REQUIRE(true == array1.get(0).array->get(0).value.b);
  REQUIRE(4 == array1.get(0).array->get(1).value.i32);
}

TEST_CASE("MultiTypeArray-nested-more-push-back", "[MULTITYPEARRAY]")
{
  MultiTypeArray array0;
  MultiTypeArray array1;
  MultiTypeArray array2;
  
  array0.push_back(array1);
  array1.push_back(array2);
  array2.push_back(42);

  REQUIRE(42 == array0.get(0).array->get(0).array->get(0).value.i32);
}
