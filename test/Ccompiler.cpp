#include <sstream>
#include <catch2/catch.hpp>
#include <Ccompiler.hpp>
#include <Lexer.hpp>
#include <Parser.hpp>
#include <TreeNodes.hpp>
#include <ThreeAddressCode.hpp>
#include <Env.hpp>

TEST_CASE("Ccompiler-simple", "[CCOMPILER]")
{
  SECTION("int-arith")
    {
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& node = const_cast<tree::Node&>( parser("1 + 2 - 3 * 4 / 5 % 6;") );
      Env env;
      tree::ThreeAddressCode tac {&env}; node.visit(tac);
      
      Ccompiler compiler { tac };

      std::stringstream output;
      output << "Push(\"6\");" << std::endl;
      output << "Push(\"5\");" << std::endl;
      output << "Push(\"4\");" << std::endl;
      output << "Push(\"3\");" << std::endl;
      output << "Mul();" << std::endl;
      output << "Div();" << std::endl;
      output << "Mod();" << std::endl;
      output << "Push(\"2\");" << std::endl;
      output << "Push(\"1\");" << std::endl;
      output << "Add();" << std::endl;
      output << "Sub();" << std::endl;
      
      REQUIRE(output.str() == compiler.source());
    }

  SECTION("bool-arith")
    {
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& node = const_cast<tree::Node&>( parser("true && false || true && !false;") );
      Env env;
      tree::ThreeAddressCode tac {&env}; node.visit(tac);
      
      Ccompiler compiler { tac };

      std::stringstream output;
      output << "Push(\"0\");" << std::endl;
      output << "Not();" << std::endl;
      output << "Push(\"1\");" << std::endl;
      output << "Push(\"0\");" << std::endl;
      output << "Push(\"1\");" << std::endl;
      output << "And();" << std::endl;
      output << "Or();" << std::endl;
      output << "And();" << std::endl;

      REQUIRE(output.str() == compiler.source());
    }
}

TEST_CASE("Ccompiler-assert", "[CCOMPILER]")
{
  Lexer lexer;
  Parser parser { lexer };
  tree::Node& node = const_cast<tree::Node&>( parser("_assert 5 + 2;") );
  Env env;
  tree::ThreeAddressCode tac { &env }; node.visit(tac);
      
  Ccompiler compiler { tac };

  std::stringstream output;
  output << "Push(\"2\");" << std::endl;
  output << "Push(\"5\");" << std::endl;
  output << "Add();" << std::endl;
  output << "Assert();" << std::endl;
  
  REQUIRE(output.str() == compiler.source());
}

TEST_CASE("Ccompiler-dump", "[CCOMPILER]")
{
  Lexer lexer;
  Parser parser { lexer };
  tree::Node& node = const_cast<tree::Node&>( parser("_dump 1 - 7;") );
  Env env;
  tree::ThreeAddressCode tac { &env }; node.visit(tac);
      
  Ccompiler compiler { tac };

  std::stringstream output;
  output << "Push(\"7\");" << std::endl;
  output << "Push(\"1\");" << std::endl;
  output << "Sub();" << std::endl;
  output << "Dump();" << std::endl;
  
  REQUIRE(output.str() == compiler.source());
}

TEST_CASE("Ccompiler-cmp", "[CCOMPILER]")
{
  SECTION("lt")
    {
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& node = const_cast<tree::Node&>( parser("1 < 2;") );
      Env env;
      tree::ThreeAddressCode tac { &env }; node.visit(tac);
      
      Ccompiler compiler { tac };

      std::stringstream output;
      output << "Push(\"2\");" << std::endl;
      output << "Push(\"1\");" << std::endl;
      output << "Lt();" << std::endl;
  
      REQUIRE(output.str() == compiler.source());
    }

  SECTION("le")
    {
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& node = const_cast<tree::Node&>( parser("1 <= 2;") );
      Env env;
      tree::ThreeAddressCode tac { &env }; node.visit(tac);
      
      Ccompiler compiler { tac };

      std::stringstream output;
      output << "Push(\"2\");" << std::endl;
      output << "Push(\"1\");" << std::endl;
      output << "Le();" << std::endl;
  
      REQUIRE(output.str() == compiler.source());
    }

  SECTION("gt")
    {
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& node = const_cast<tree::Node&>( parser("1 > 2;") );
      Env env;
      tree::ThreeAddressCode tac { &env }; node.visit(tac);
      
      Ccompiler compiler { tac };

      std::stringstream output;
      output << "Push(\"2\");" << std::endl;
      output << "Push(\"1\");" << std::endl;
      output << "Gt();" << std::endl;
  
      REQUIRE(output.str() == compiler.source());
    }

  SECTION("Ge")
    {
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& node = const_cast<tree::Node&>( parser("1 >= 2;") );
      Env env;
      tree::ThreeAddressCode tac { &env }; node.visit(tac);
      
      Ccompiler compiler { tac };

      std::stringstream output;
      output << "Push(\"2\");" << std::endl;
      output << "Push(\"1\");" << std::endl;
      output << "Ge();" << std::endl;
  
      REQUIRE(output.str() == compiler.source());
    }
}


