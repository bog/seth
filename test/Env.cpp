#include <catch2/catch.hpp>
#include <Env.hpp>
#include <Type.hpp>
#include <MsgStatus.hpp>

TEST_CASE("Env-vardecl-varget-vtype", "[ENV]")
{
  MsgStatus::init();
  
  Env env;
  env.declare("u", Type::Int, "-4");
  env.declare("v", Type::Bool, "false");
  env.declare("w", Type::Int, "0");
  env.declare("x", Type::Int, "32");
  env.declare("y", Type::Int, "43");
  env.declare("z", Type::Bool, "true");
  
  REQUIRE( "-4" == env.get("u") );
  REQUIRE( "false" == env.get("v") );
  REQUIRE( "0" == env.get("w") );
  REQUIRE( "32" == env.get("x") );
  REQUIRE( "43" == env.get("y") );
  REQUIRE( "true" == env.get("z") );

  REQUIRE( Type::Int == env.type("u") );
  REQUIRE( Type::Bool == env.type("v") );
  REQUIRE( Type::Int == env.type("w") );
  REQUIRE( Type::Int == env.type("x") );
  REQUIRE( Type::Int == env.type("y") );
  REQUIRE( Type::Bool == env.type("z") );

  REQUIRE( MsgStatus::errors_str().empty() );
}

TEST_CASE("Env-err-multi-vardecl", "[ENV]")
{
  MsgStatus::init();
  
  Env env;
  env.declare("x", Type::Int, "42");
  env.declare("x", Type::Int, "47");
  env.declare("z", Type::Int, "0");

  std::vector<std::string> errs = MsgStatus::errors_str();

  REQUIRE(1 == errs.size());
}

TEST_CASE("Env-err-doesnot-exists", "[ENV]")
{
  MsgStatus::init();
  
  Env env;

  REQUIRE( std::nullopt == env.get("x") );
  
  std::vector<std::string> errs = MsgStatus::errors_str();

  REQUIRE(1 == errs.size());
}

TEST_CASE("Env-contains", "[ENV]")
{
  MsgStatus::init();
  
  Env env;

  env.declare("x", Type::Int, "42");
  
  REQUIRE( MsgStatus::errors_str().empty() );
  REQUIRE( env.contains("x") );
  REQUIRE( !env.contains("y") );  
}

TEST_CASE("Env-new-block", "[ENV]")
{
  Env env;
  REQUIRE( !env.contains("x") );
  env.declare("x", Type::Int, "6");
  REQUIRE( env.contains("x") );
  
  env.block();
  env.declare("y", Type::Int, "7");
  REQUIRE( env.contains("y") );
  
  env.unblock();
  REQUIRE( env.contains("x") );
  REQUIRE( !env.contains("y") );
}

TEST_CASE("Env-assign", "[ENV]")
{
  Env env;

  env.declare("x", Type::Int, "42");
  REQUIRE("42" == *env.get("x"));
  env.set("x", "34");
  REQUIRE("34" == *env.get("x"));
}


TEST_CASE("Env-same-name-different-scope", "[ENV]")
{
  Env env;

  env.declare("x", Type::Int, "42");
  REQUIRE("42" == *env.get("x"));
  
  env.block();
  env.declare("x", Type::Int, "27");
  REQUIRE("27" == *env.get("x"));
  env.unblock();
  
  REQUIRE("42" == *env.get("x"));
}
