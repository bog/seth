#include <sstream>
#include <catch2/catch.hpp>
#include <Lexer.hpp>
#include <Parser.hpp>
#include <TreeNodes.hpp>
#include <Type.hpp>

TEST_CASE("Parser-int", "[PARSER]")
{
  Lexer lexer;  
  Parser parser(lexer);

  REQUIRE( tree::Int(0) == parser("0;") );
  REQUIRE( tree::Int(5) == parser("5;") );
  REQUIRE( tree::Int(235) == parser("235;") );
  REQUIRE( tree::Usub( new tree::Int(12) ) == parser("-12;") );
}

TEST_CASE("Parser-int-arith", "[PARSER]")
{
  Lexer lexer;  
  Parser parser(lexer);
  
  REQUIRE( tree::Add( new tree::Int(5), new tree::Int(7) ) == parser("5 + 7;") );
  REQUIRE( tree::Sub( new tree::Int(32),new tree::Int(154) ) == parser("32 - 154;") );
  REQUIRE( tree::Sub( new tree::Usub(new tree::Int(4)),new tree::Int(7) ) == parser("-4 - 7;") );
  REQUIRE( tree::Mul( new tree::Int(27),new tree::Usub(new tree::Int(4)) ) == parser("27 * -4;") );
  REQUIRE( tree::Div( new tree::Int(1), new tree::Int(50) ) == parser("1 / 50;") );
  REQUIRE( tree::Mod( new tree::Int(17),new tree::Int(6) ) == parser("17 % 6;") );

  REQUIRE( tree::Add( new tree::Sub( new tree::Add(new tree::Int(1), new tree::Int(2)), new tree::Int(3)), new tree::Int(4))		      
	   == parser("1 + 2 - 3 + 4;") );
}

TEST_CASE("Parser-cmp", "[PARSER]")
{
  Lexer lexer;  
  Parser parser(lexer);

  REQUIRE( tree::Lt ( new tree::Int(45), new tree::Int(63) ) == parser("45 < 63;") );
  REQUIRE( tree::Le ( new tree::Int(0), new tree::Usub(new tree::Int(27)) ) == parser("0 <= -27;") );
  REQUIRE( tree::Leq ( new tree::Int(1), new tree::Int(12) ) == parser("1 <== 12;") );

  REQUIRE( tree::Gt ( new tree::Int(12), new tree::Usub(new tree::Int(7)) ) == parser("12 > -7;") );
  REQUIRE( tree::Ge ( new tree::Int(2), new  tree::Int(3) ) == parser("2 >= 3;") );
  REQUIRE( tree::Geq (new  tree::Int(3014), new  tree::Int(1237) ) == parser("3014 >== 1237;") );

  REQUIRE( tree::Equal ( new tree::Int(21), new tree::Int(43) ) == parser("21 == 43;") );
  REQUIRE( tree::Equiv ( new tree::Int(29), new tree::Int(234) ) == parser("29 === 234;") );
  REQUIRE( tree::Ne ( new tree::Int(121), new  tree::Int(413) ) == parser("121 != 413;") );
  REQUIRE( tree::Neq ( new tree::Int(291), new  tree::Int(2314) ) == parser("291 !== 2314;") );
}

TEST_CASE("Parser-bool", "[PARSER]")
{
  Lexer lexer;  
  Parser parser(lexer);

  REQUIRE( tree::Bool(true) == parser("true;") );
  REQUIRE( tree::Bool(false) == parser("false;") );
}

TEST_CASE("Parser-bool-arith", "[PARSER]")
{
  Lexer lexer;  
  Parser parser(lexer);

  REQUIRE( tree::And( new tree::Bool(true), new tree::Bool(false) ) == parser("true && false;") );
  REQUIRE( tree::Or( new tree::Bool(false), new tree::Bool(true) ) == parser("false || true;") );
  REQUIRE( tree::Not( new tree::Bool(true) ) == parser("!true;") );

  REQUIRE( tree::And(new tree::Or(new tree::And(new tree::Bool(false), new tree::Bool(true)), new tree::Bool(false)), new tree::Bool(true))
	   == parser("false && true || false && true;") );
}

TEST_CASE("Parser-dump", "[PARSER]")
{

  SECTION("with-int")
    {
      Lexer lexer;      
      Parser parser(lexer);

      REQUIRE( tree::Dump( new tree::Int(42) ) == parser("_dump 42;") );
      REQUIRE( tree::Dump( new tree::Usub(new tree::Int(34)) ) == parser("_dump -34;") );
    }
  
}

TEST_CASE("Parser-assert", "[PARSER]")
{

  SECTION("with-int")
    {
      Lexer lexer;      
      Parser parser(lexer);

      REQUIRE( tree::Assert( new tree::Int(42) ) == parser("_assert 42;") );
      REQUIRE( tree::Assert( new tree::Usub(new tree::Int(34)) ) == parser("_assert -34;") );
    }  
}

TEST_CASE("Parser-bug-par0", "[PARSER]")
{
  Lexer lexer;  
  Parser parser(lexer);

  REQUIRE( tree::Not(
		     new tree::And(
				   new tree::Lt(new tree::Int(5), new tree::Int(10)),
				   new tree::Equal(new tree::Int(1), new tree::Int(1))))
				    
	   == parser("!(5 < 10 && 1 == 1);") );
}

TEST_CASE("Parser-vardecl", "[PARSER]")
{
  SECTION("int")
    {
      Lexer lexer;  
      Parser parser(lexer);
      tree::Node& n = const_cast<tree::Node&>( parser("x : int = 4;") );

      REQUIRE(tree::VarDecl(new tree::Ident("x"), Type::Int, new tree::Int(4)) == n);
    }

  SECTION("bool")
    {
      Lexer lexer;  
      Parser parser(lexer);
      tree::Node& n = const_cast<tree::Node&>( parser("y : bool = false;") );

      REQUIRE(tree::VarDecl(new tree::Ident("y"), Type::Bool, new tree::Bool(false)) == n);
    }

  SECTION("unknown0")
    {
      Lexer lexer;  
      Parser parser(lexer);
      tree::Node& n = const_cast<tree::Node&>( parser("z : hello = 4;") );

      REQUIRE(tree::VarDecl(new tree::Ident("z"), Type::Invalid, new tree::Int(4)) == n);
    }  
}

TEST_CASE("Parser-block0", "[PARSER]")
{
  Lexer lexer;  
  Parser parser(lexer);

  REQUIRE(
	  tree::Block(
		      new tree::Seq(
				    new tree::Seq(new tree::VarDecl(new tree::Ident("x"),
								    Type::Int,
								    new tree::Int(4)),
						  new tree::Dump( new tree::Ident("x"))),
				    nullptr))
				    

				    
	  == parser("{ x : int = 4; _dump x; }") );
}

TEST_CASE("Parser-block1", "[PARSER]")
{
  Lexer lexer;  
  Parser parser(lexer);

  REQUIRE(
	  tree::Block( new tree::Seq(new tree::Seq(new tree::Seq(new tree::Seq(new tree::Int(0), new tree::Int(1)),
								 new tree::Int(2)),
						   new tree::Int(3)),
				     nullptr))

						   
		      
		      
	  == parser("{ 0; 1; 2; 3; }") );
}


TEST_CASE("Parser-two-block", "[PARSER]")
{
  Lexer lexer;  
  Parser parser(lexer);

  REQUIRE( tree::Seq(
		     new tree::Block(
				     new tree::Seq(new tree::Seq(new tree::Seq(new tree::Int(0), new tree::Int(1)),
								 new tree::Int(2)),
						   nullptr)),
		     new tree::Block(
				     new tree::Seq(new tree::Seq(new tree::Seq(new tree::Int(3), new tree::Int(4)),
								 new tree::Int(5)),
						   nullptr)))
	  
	   == parser("{ 0; 1; 2; }; {3; 4; 5;}") );
}

TEST_CASE("Parser-inner-block", "[PARSER]")
{
  Lexer lexer;  
  Parser parser(lexer);
  
  REQUIRE(
	  tree::Block(
		      new tree::Seq( new tree::Int(0),
				     new tree::Block(new tree::Seq(new tree::Seq(new tree::Int(1),
										 new tree::Int(2)),
								   nullptr))))
	  
				    
				    
	  == parser("{ 0; { 1; 2; } }") );
}

TEST_CASE("Parser-assign", "[PARSER]")
{
  SECTION("simple-int")
    {
      Lexer lexer;  
      Parser parser(lexer);
  
      REQUIRE( tree::Assign(new tree::Ident("x"), new tree::Int(5)) == parser("x = 5") );
    }
}

TEST_CASE("Parser-if", "[PARSER]")
{
  SECTION("simple-if")
    {
      Lexer lexer;  
      Parser parser(lexer);

      std::stringstream source;
      source << "if {" << std::endl;
      source << "(x == 4) { _dump 0 }" << std::endl;
      source << "(y == 2) { _dump 1 }" << std::endl;
      source << "(else) { _dump 2 }" << std::endl;
      source << "}" << std::endl;
      
      REQUIRE( tree::If(new tree::Seq(new tree::Seq(
						    
			new tree::IfEntry(new tree::Equal(new tree::Ident("x"), new tree::Int(4)),
					  new tree::Block(new tree::Dump(new tree::Int(0)))),
				      
			new tree::IfEntry(new tree::Equal(new tree::Ident("y"), new tree::Int(2)),
					  new tree::Block(new tree::Dump(new tree::Int(1))))),
				      
			new tree::IfEntry(nullptr,
					  new tree::Block(new tree::Dump(new tree::Int(2))))))
							
	       == parser(source.str()) );
    }

  SECTION("if-add-int")
    {
      Lexer lexer;  
      Parser parser(lexer);

      std::stringstream source;
      source << " 5 + (if {" << std::endl;
      source << "(x == 4) { _dump 0 }" << std::endl;
      source << "(y == 2) { _dump 1 }" << std::endl;
      source << "(else) { _dump 2 }" << std::endl;
      source << "})" << std::endl;
      
      REQUIRE( tree::Add(new tree::Int(5),
			 new tree::If(new tree::Seq(new tree::Seq(
						    
								  new tree::IfEntry(new tree::Equal(new tree::Ident("x"), new tree::Int(4)),
										    new tree::Block(new tree::Dump(new tree::Int(0)))),
				      
								  new tree::IfEntry(new tree::Equal(new tree::Ident("y"), new tree::Int(2)),
										    new tree::Block(new tree::Dump(new tree::Int(1))))),
				      
						    new tree::IfEntry(nullptr,
								      new tree::Block(new tree::Dump(new tree::Int(2)))))))
							
			 == parser(source.str()) );
	       }
    }


TEST_CASE("Parser-loop", "[PARSER]")
{
  SECTION("simple-loop")
    {
      Lexer lexer;  
      Parser parser(lexer);

      std::stringstream source;
      source << "loop x < 10 {" << std::endl;
      source << "_dump x;" << std::endl;
      source << "x = x + 1;" << std::endl;
      source << "}" << std::endl;
      
      REQUIRE( tree::Loop(new tree::Lt(new tree::Ident("x"), new tree::Int(10)),
			  new tree::Seq(new tree::Seq(new tree::Dump(new tree::Ident("x")),
						      new tree::Assign(new tree::Ident("x"),
								       new tree::Add(new tree::Ident("x"),
										     new tree::Int(1)))),
					nullptr))
	       
							
	       == parser(source.str()) );
    }
}


TEST_CASE("Parser-array", "[PARSER]")
{
  SECTION("simple-array-literal")
    {
      Lexer lexer;  
      Parser parser(lexer);

      std::stringstream source;
      source << "[false, 3, 1, 4, true]" << std::endl;
      
      REQUIRE(tree::Array(new tree::Seq(new tree::Seq(new tree::Seq( new tree::Seq(new tree::Bool(false),
										   new tree::Int(3)),
								     new tree::Int(1)),
						      new tree::Int(4)),
					new tree::Bool(true)))
							
	       == parser(source.str()) );
    }

    SECTION("empty-array-literal")
    {
      Lexer lexer;  
      Parser parser(lexer);

      std::stringstream source;
      source << "[]" << std::endl;
      
      REQUIRE(tree::Array(nullptr)							
	       == parser(source.str()) );
    }
}
