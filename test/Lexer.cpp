#include <sstream>
#include <iostream>
#include <catch2/catch.hpp>
#include <Lexer.hpp>

TEST_CASE("Lexer-number", "[LEXER]")
{
  SECTION("one-digit")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer("5");
      REQUIRE(1 == tokens.size());
      REQUIRE(TokenType::Int == tokens[0].type);
      REQUIRE(5 == tokens[0].value.i32);
    }

    SECTION("more-digit")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer(" 345  ");
      REQUIRE(1 == tokens.size());
      REQUIRE(TokenType::Int == tokens[0].type);
      REQUIRE(345 == tokens[0].value.i32);
    }

    SECTION("negative-digit")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer(" - 13  ");
      REQUIRE(2 == tokens.size());
      REQUIRE(TokenType::Sub == tokens[0].type);
      REQUIRE(TokenType::Int == tokens[1].type);
      REQUIRE(13 == tokens[1].value.i32);
    }
}


TEST_CASE("Lexer-op", "[LEXER]")
{
  SECTION("add")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer("37 + 4");
      
      REQUIRE(3 == tokens.size());
      
      REQUIRE(TokenType::Int == tokens[0].type);
      REQUIRE(37 == tokens[0].value.i32);
      
      REQUIRE(TokenType::Add == tokens[1].type);
      
      REQUIRE(TokenType::Int == tokens[2].type);
      REQUIRE(4 == tokens[2].value.i32);
    }

    SECTION("add-no-space")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer("2+45");
      
      REQUIRE(3 == tokens.size());
      
      REQUIRE(TokenType::Int == tokens[0].type);
      REQUIRE(2 == tokens[0].value.i32);
      
      REQUIRE(TokenType::Add == tokens[1].type);
      
      REQUIRE(TokenType::Int == tokens[2].type);
      REQUIRE(45 == tokens[2].value.i32);
    }

    SECTION("sub")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer("365-78 ");
      
      REQUIRE(3 == tokens.size());
      
      REQUIRE(TokenType::Int == tokens[0].type);
      REQUIRE(365 == tokens[0].value.i32);
      
      REQUIRE(TokenType::Sub == tokens[1].type);
      
      REQUIRE(TokenType::Int == tokens[2].type);
      REQUIRE(78 == tokens[2].value.i32);
    }

    SECTION("usub")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer("-44");
      
      REQUIRE(2 == tokens.size());
      
      REQUIRE(TokenType::Sub == tokens[0].type);            
      REQUIRE(TokenType::Int == tokens[1].type);
      REQUIRE(44 == tokens[1].value.i32);
    }

    SECTION("mul")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer(" 6 *7");
      
      REQUIRE(3 == tokens.size());
      
      REQUIRE(TokenType::Int == tokens[0].type);
      REQUIRE(6 == tokens[0].value.i32);
      
      REQUIRE(TokenType::Mul == tokens[1].type);
      
      REQUIRE(TokenType::Int == tokens[2].type);
      REQUIRE(7 == tokens[2].value.i32);
    }

    SECTION("div")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer("1234/ 456");
      
      REQUIRE(3 == tokens.size());
      
      REQUIRE(TokenType::Int == tokens[0].type);
      REQUIRE(1234 == tokens[0].value.i32);
      
      REQUIRE(TokenType::Div == tokens[1].type);
      
      REQUIRE(TokenType::Int == tokens[2].type);
      REQUIRE(456 == tokens[2].value.i32);
    }

    SECTION("mod")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer("37%   4");
      
      REQUIRE(3 == tokens.size());
      
      REQUIRE(TokenType::Int == tokens[0].type);
      REQUIRE(37 == tokens[0].value.i32);
      
      REQUIRE(TokenType::Mod == tokens[1].type);
      
      REQUIRE(TokenType::Int == tokens[2].type);
      REQUIRE(4 == tokens[2].value.i32);
    }
}

TEST_CASE("Lexer-bool", "[LEXER]")
{
  SECTION("true")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer("true");
      REQUIRE(1 == tokens.size());
      REQUIRE(TokenType::True == tokens[0].type);
      REQUIRE(true == tokens[0].value.b);
    }

  SECTION("false")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer("false");
      REQUIRE(1 == tokens.size());
      REQUIRE(TokenType::False == tokens[0].type);
      REQUIRE(false == tokens[0].value.b);
    }

  SECTION("not")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer("!true");
      REQUIRE(2 == tokens.size());
      REQUIRE(TokenType::Not == tokens[0].type);

      REQUIRE(TokenType::True == tokens[1].type);
      REQUIRE(true == tokens[1].value.b);
    }

    SECTION("formula")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer("true && false || true && (true || false)");
      REQUIRE(11 == tokens.size());

      REQUIRE(TokenType::True == tokens[0].type);
      REQUIRE(true == tokens[0].value.b);
      
      REQUIRE(TokenType::And == tokens[1].type);

      REQUIRE(TokenType::False == tokens[2].type);
      REQUIRE(false == tokens[2].value.b);

      REQUIRE(TokenType::Or == tokens[3].type);

      REQUIRE(TokenType::True == tokens[4].type);
      REQUIRE(true == tokens[4].value.b);

      REQUIRE(TokenType::And == tokens[5].type);

      REQUIRE(TokenType::OpenPar == tokens[6].type);

      REQUIRE(TokenType::True == tokens[7].type);
      REQUIRE(true == tokens[7].value.b);

      REQUIRE(TokenType::Or == tokens[8].type);

      REQUIRE(TokenType::False == tokens[9].type);
      REQUIRE(false == tokens[9].value.b);

      REQUIRE(TokenType::ClosePar == tokens[10].type);
    }
}

TEST_CASE("Lexer-helpers", "[LEXER]")
{
  SECTION("_dump-int")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer("_dump 534");
      REQUIRE(2 == tokens.size());
      
      REQUIRE(TokenType::Dump == tokens[0].type);
      
      REQUIRE(TokenType::Int == tokens[1].type);
      REQUIRE(534 == tokens[1].value.i32);
    }

    SECTION("_dump-bool")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer("_dump true");
      REQUIRE(2 == tokens.size());
      
      REQUIRE(TokenType::Dump == tokens[0].type);
      
      REQUIRE(TokenType::True == tokens[1].type);
      REQUIRE(true == tokens[1].value.b);
    }

    SECTION("_assert-int")
      {
	Lexer lexer;
	std::vector<Token> tokens = lexer("_assert 212");
	REQUIRE(2 == tokens.size());
      
	REQUIRE(TokenType::Assert == tokens[0].type);
      
	REQUIRE(TokenType::Int == tokens[1].type);
	REQUIRE(212 == tokens[1].value.i32);
      }

    SECTION("_assert-bool")
      {
	Lexer lexer;
	std::vector<Token> tokens = lexer("_assert true");
	REQUIRE(2 == tokens.size());
      
	REQUIRE(TokenType::Assert == tokens[0].type);
      
	REQUIRE(TokenType::True == tokens[1].type);
	REQUIRE(true == tokens[1].value.b);
      }
}

TEST_CASE("Lexer-comparisons", "[LEXER]")
{
  SECTION("all-comparisons")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer("> >= >== < <= <== == === != !==");
      REQUIRE(10 == tokens.size());
      
      REQUIRE(TokenType::Gt == tokens[0].type);
      REQUIRE(TokenType::Ge == tokens[1].type);
      REQUIRE(TokenType::Geq == tokens[2].type);
      REQUIRE(TokenType::Lt == tokens[3].type);
      REQUIRE(TokenType::Le == tokens[4].type);
      REQUIRE(TokenType::Leq == tokens[5].type);
      REQUIRE(TokenType::Equal == tokens[6].type);
      REQUIRE(TokenType::Equiv == tokens[7].type);
      REQUIRE(TokenType::Ne == tokens[8].type);
      REQUIRE(TokenType::Neq == tokens[9].type);
    }
}

TEST_CASE("Lexer-vardecl", "[LEXER]")
{
  SECTION("int-var")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer("x : int = 42");
      REQUIRE(5 == tokens.size());
      
      REQUIRE(TokenType::Ident == tokens[0].type);
      REQUIRE(TokenType::Colon == tokens[1].type);
      REQUIRE(TokenType::Type == tokens[2].type);
      REQUIRE(TokenType::Assign == tokens[3].type);
      REQUIRE(TokenType::Int == tokens[4].type);
    }

  SECTION("bool-false-var")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer("y : bool = false");
      REQUIRE(5 == tokens.size());
      
      REQUIRE(TokenType::Ident == tokens[0].type);
      REQUIRE(TokenType::Colon == tokens[1].type);
      REQUIRE(TokenType::Type == tokens[2].type);
      REQUIRE(TokenType::Assign == tokens[3].type);
      REQUIRE(TokenType::False == tokens[4].type);
    }

    SECTION("bool-true-var")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer("y : bool = true");
      REQUIRE(5 == tokens.size());
      
      REQUIRE(TokenType::Ident == tokens[0].type);
      REQUIRE(TokenType::Colon == tokens[1].type);
      REQUIRE(TokenType::Type == tokens[2].type);
      REQUIRE(TokenType::Assign == tokens[3].type);
      REQUIRE(TokenType::True == tokens[4].type);
    }

    SECTION("unknown-type")
      {
	Lexer lexer;
	std::vector<Token> tokens = lexer("x : gozak = 42");
	REQUIRE(5 == tokens.size());
      
	REQUIRE(TokenType::Ident == tokens[0].type);
	REQUIRE(TokenType::Colon == tokens[1].type);
	REQUIRE(TokenType::Ident == tokens[2].type);
	REQUIRE("gozak" == tokens[2].inspect);
	REQUIRE(TokenType::Assign == tokens[3].type);
	REQUIRE(TokenType::Int == tokens[4].type);
      }

}

TEST_CASE("Lexer-block", "[LEXER]")
{
  SECTION("two-blocks")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer(" { 1; 2; 3; } {4; 5; 6;} ");
      
      REQUIRE(16 == tokens.size());
      
      REQUIRE(TokenType::OpenBlock == tokens[0].type);
      REQUIRE(TokenType::Int == tokens[1].type);
      REQUIRE(TokenType::SemiColon == tokens[2].type);
      REQUIRE(TokenType::Int == tokens[3].type);
      REQUIRE(TokenType::SemiColon == tokens[4].type);
      REQUIRE(TokenType::Int == tokens[5].type);
      REQUIRE(TokenType::SemiColon == tokens[6].type);
      REQUIRE(TokenType::CloseBlock == tokens[7].type);

      REQUIRE(TokenType::OpenBlock == tokens[8].type);
      REQUIRE(TokenType::Int == tokens[9].type);
      REQUIRE(TokenType::SemiColon == tokens[10].type);
      REQUIRE(TokenType::Int == tokens[11].type);
      REQUIRE(TokenType::SemiColon == tokens[12].type);
      REQUIRE(TokenType::Int == tokens[13].type);
      REQUIRE(TokenType::SemiColon == tokens[14].type);
      REQUIRE(TokenType::CloseBlock == tokens[15].type);
    }
}

TEST_CASE("Lexer-if", "[LEXER]")
{
  SECTION("if-cond")
    {
      Lexer lexer;
      std::stringstream source;
      source << "if {" << std::endl;
      source << "(x == 3) { _dump 1 }" << std::endl;
      source << "(y == 0) { _dump 2 }" << std::endl;
      source << "(else) { _dump 3 }" << std::endl;
      source << "}" << std::endl;
      
      std::vector<Token> tokens = lexer(source.str());
      REQUIRE(28 == tokens.size());
      
      REQUIRE(TokenType::If == tokens[0].type);
      REQUIRE(TokenType::OpenBlock == tokens[1].type);
      
      // (x == 3) { _dump 1 }
      REQUIRE(TokenType::OpenPar == tokens[2].type); 
      REQUIRE(TokenType::Ident == tokens[3].type);
      REQUIRE(TokenType::Equal == tokens[4].type);
      REQUIRE(TokenType::Int == tokens[5].type);      
      REQUIRE(3 == tokens[5].value.i32);
      REQUIRE(TokenType::ClosePar == tokens[6].type); 
      REQUIRE(TokenType::OpenBlock == tokens[7].type);
      REQUIRE(TokenType::Dump == tokens[8].type);
      REQUIRE(TokenType::Int == tokens[9].type);
      REQUIRE(1 == tokens[9].value.i32);
      REQUIRE(TokenType::CloseBlock == tokens[10].type);

      // (y == 0) { _dump 2 }
      REQUIRE(TokenType::OpenPar == tokens[11].type); 
      REQUIRE(TokenType::Ident == tokens[12].type);
      REQUIRE(TokenType::Equal == tokens[13].type);
      REQUIRE(TokenType::Int == tokens[14].type);
      REQUIRE(0 == tokens[14].value.i32);
      REQUIRE(TokenType::ClosePar == tokens[15].type); 
      REQUIRE(TokenType::OpenBlock == tokens[16].type);
      REQUIRE(TokenType::Dump == tokens[17].type);
      REQUIRE(TokenType::Int == tokens[18].type);
      REQUIRE(2 == tokens[18].value.i32);
      REQUIRE(TokenType::CloseBlock == tokens[19].type);

      // (else) { _dump 3 }
      REQUIRE(TokenType::OpenPar == tokens[20].type); 
      REQUIRE(TokenType::Else == tokens[21].type);
      REQUIRE(TokenType::ClosePar == tokens[22].type); 
      REQUIRE(TokenType::OpenBlock == tokens[23].type);
      REQUIRE(TokenType::Dump == tokens[24].type);
      REQUIRE(TokenType::Int == tokens[25].type);
      REQUIRE(3 == tokens[25].value.i32);
      REQUIRE(TokenType::CloseBlock == tokens[26].type);
      
      REQUIRE(TokenType::CloseBlock == tokens[27].type);
    }
}

TEST_CASE("Lexer-number-loop", "[LEXER]")
{
  SECTION("simple")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer("loop x < 10 { x = x + 1 }");
      REQUIRE(11 == tokens.size());
      REQUIRE(TokenType::Loop == tokens[0].type);
      REQUIRE(TokenType::Ident == tokens[1].type);
      REQUIRE(TokenType::Lt == tokens[2].type);
      REQUIRE(TokenType::Int == tokens[3].type);
      REQUIRE(10 == tokens[3].value.i32);
      REQUIRE(TokenType::OpenBlock == tokens[4].type);
      REQUIRE(TokenType::Ident == tokens[5].type);
      REQUIRE(TokenType::Assign == tokens[6].type);
      REQUIRE(TokenType::Ident == tokens[7].type);
      REQUIRE(TokenType::Add == tokens[8].type);
      REQUIRE(TokenType::Int == tokens[9].type);
      REQUIRE(1 == tokens[9].value.i32);
      REQUIRE(TokenType::CloseBlock == tokens[10].type);
    }
}

TEST_CASE("Lexer-array", "[LEXER]")
{
  SECTION("simple")
    {
      Lexer lexer;
      std::vector<Token> tokens = lexer("[true, false, 1, 3, true]");
      
      REQUIRE(11 == tokens.size());
      
      REQUIRE(TokenType::OpenArray == tokens[0].type);
      REQUIRE(TokenType::True == tokens[1].type);
      REQUIRE(TokenType::Comma == tokens[2].type);
      REQUIRE(TokenType::False == tokens[3].type);
      REQUIRE(TokenType::Comma == tokens[4].type);      
      REQUIRE(TokenType::Int == tokens[5].type);
      REQUIRE(1 == tokens[5].value.i32);
      REQUIRE(TokenType::Comma == tokens[6].type);
      REQUIRE(TokenType::Int == tokens[7].type);
      REQUIRE(3 == tokens[7].value.i32);
      REQUIRE(TokenType::Comma == tokens[8].type);
      REQUIRE(TokenType::True == tokens[9].type);
      REQUIRE(TokenType::CloseArray == tokens[10].type);
    }
}
