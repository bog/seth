#include <sstream>
#include <iostream>
#include <catch2/catch.hpp>
#include <ThreeAddressCode.hpp>
#include <Parser.hpp>
#include <Lexer.hpp>
#include <TreeNodes.hpp>
#include <Env.hpp>
#include <ValueOf.hpp>

TEST_CASE("ThreeAddressCode-simple-int", "[THREEADDRESSCODE]")
{
  Lexer lexer;
  Parser parser { lexer };
  Env env;
  tree::ThreeAddressCode tac { &env };  
  tree::Node& node = const_cast<tree::Node&>( parser("462;") );
  node.visit(tac);
  std::vector<tree::tac_t> instructions = tac.instructions();  
  REQUIRE( 1 == instructions.size() );
  REQUIRE(tree::tac_t(tree::OpCode::Push, "462", std::nullopt) == instructions[0]);
}

TEST_CASE("ThreeAddressCode-add-int", "[THREEADDRESSCODE]")
{
  Lexer lexer;
  Parser parser { lexer };
  Env env;
  tree::ThreeAddressCode tac { &env };  
  tree::Node& node = const_cast<tree::Node&>( parser("3 + 7;") );
  node.visit(tac);
  std::vector<tree::tac_t> instructions = tac.instructions();  
  REQUIRE( 3 == instructions.size() );
  
  REQUIRE(tree::tac_t(tree::OpCode::Push, "7", std::nullopt) == instructions[0]);
  REQUIRE(tree::tac_t(tree::OpCode::Push, "3", std::nullopt) == instructions[0]);
  REQUIRE(tree::tac_t(tree::OpCode::Add, std::nullopt, std::nullopt) == instructions[0]);
}

TEST_CASE("ThreeAddressCode-sub-int", "[THREEADDRESSCODE]")
{
  Lexer lexer;
  Parser parser { lexer };
  Env env;
  tree::ThreeAddressCode tac { &env };  
  tree::Node& node = const_cast<tree::Node&>( parser("34 - 9;") );
  node.visit(tac);
  std::vector<tree::tac_t> instructions = tac.instructions();  
  REQUIRE( 3 == instructions.size() );
  
  REQUIRE(tree::tac_t(tree::OpCode::Push, "9", std::nullopt) == instructions[0]);
  REQUIRE(tree::tac_t(tree::OpCode::Push, "34", std::nullopt) == instructions[1]);
  REQUIRE(tree::tac_t(tree::OpCode::Sub, std::nullopt, std::nullopt) == instructions[2]);
}

TEST_CASE("ThreeAddressCode-mul-int", "[THREEADDRESSCODE]")
{
  Lexer lexer;
  Parser parser { lexer };
  Env env;
  tree::ThreeAddressCode tac { &env };  
  tree::Node& node = const_cast<tree::Node&>( parser("6 *  7 ;") );
  node.visit(tac);
  std::vector<tree::tac_t> instructions = tac.instructions();  
  REQUIRE( 3 == instructions.size() );
  
  REQUIRE(tree::tac_t(tree::OpCode::Push, "7", std::nullopt) == instructions[0]);
  REQUIRE(tree::tac_t(tree::OpCode::Push, "6", std::nullopt) == instructions[1]);
  REQUIRE(tree::tac_t(tree::OpCode::Mul, std::nullopt, std::nullopt) == instructions[2]);
}

TEST_CASE("ThreeAddressCode-div-int", "[THREEADDRESSCODE]")
{
  Lexer lexer;
  Parser parser { lexer };
  Env env;
  tree::ThreeAddressCode tac { &env };  
  tree::Node& node = const_cast<tree::Node&>( parser("3 / 8;") );
  node.visit(tac);
  std::vector<tree::tac_t> instructions = tac.instructions();  
  REQUIRE( 3 == instructions.size() );
  
  REQUIRE(tree::tac_t(tree::OpCode::Push, "8", std::nullopt) == instructions[0]);
  REQUIRE(tree::tac_t(tree::OpCode::Push, "3", std::nullopt) == instructions[1]);
  REQUIRE(tree::tac_t(tree::OpCode::Div, std::nullopt, std::nullopt) == instructions[2]);
}

TEST_CASE("ThreeAddressCode-arith0-int", "[THREEADDRESSCODE]")
{
  Lexer lexer;
  Parser parser { lexer };
  Env env;
  tree::ThreeAddressCode tac { &env };  
  tree::Node& node = const_cast<tree::Node&>( parser("1 + 2 * 3;") );
  node.visit(tac);
  std::vector<tree::tac_t> instructions = tac.instructions();  
  REQUIRE( 5 == instructions.size() );
  
  REQUIRE(tree::tac_t(tree::OpCode::Push, "3", std::nullopt) == instructions[0]);
  REQUIRE(tree::tac_t(tree::OpCode::Push, "2", std::nullopt) == instructions[0]);
  REQUIRE(tree::tac_t(tree::OpCode::Mul, std::nullopt, std::nullopt) == instructions[0]);
  REQUIRE(tree::tac_t(tree::OpCode::Push, "1", std::nullopt) == instructions[0]);
  REQUIRE(tree::tac_t(tree::OpCode::Add, std::nullopt, std::nullopt) == instructions[0]);
}

TEST_CASE("ThreeAddressCode-arith1-int", "[THREEADDRESSCODE]")
{
  Lexer lexer;
  Parser parser { lexer };
  Env env;
  tree::ThreeAddressCode tac { &env };  
  tree::Node& node = const_cast<tree::Node&>( parser("1 - 2 / 3;") );
  node.visit(tac);
  std::vector<tree::tac_t> instructions = tac.instructions();  
  REQUIRE( 5 == instructions.size() );
  
  REQUIRE(tree::tac_t(tree::OpCode::Push, "3", std::nullopt) == instructions[0]);
  REQUIRE(tree::tac_t(tree::OpCode::Push, "2", std::nullopt) == instructions[0]);
  REQUIRE(tree::tac_t(tree::OpCode::Div, std::nullopt, std::nullopt) == instructions[0]);
  REQUIRE(tree::tac_t(tree::OpCode::Push, "1", std::nullopt) == instructions[0]);
  REQUIRE(tree::tac_t(tree::OpCode::Sub, std::nullopt, std::nullopt) == instructions[0]);
}

TEST_CASE("ThreeAddressCode-arith-usub-int", "[THREEADDRESSCODE]")
{
  Lexer lexer;
  Parser parser { lexer };
  Env env;
  tree::ThreeAddressCode tac { &env };  
  tree::Node& node = const_cast<tree::Node&>( parser("-4;") );
  node.visit(tac);
  std::vector<tree::tac_t> instructions = tac.instructions();  
  REQUIRE( 2 == instructions.size() );
  
  REQUIRE(tree::tac_t(tree::OpCode::Push, "4", std::nullopt) == instructions[0]);
  REQUIRE(tree::tac_t(tree::OpCode::Usub, std::nullopt, std::nullopt) == instructions[1]);
}

TEST_CASE("ThreeAddressCode-simple-bool", "[THREEADDRESSCODE]")
{
  SECTION("true")
    {
      Lexer lexer;
      Parser parser { lexer };
      Env env;
      tree::ThreeAddressCode tac { &env };  
      tree::Node& node = const_cast<tree::Node&>( parser("true;") );
      node.visit(tac);
      std::vector<tree::tac_t> instructions = tac.instructions();
      
      REQUIRE( 1 == instructions.size() );
      REQUIRE(tree::tac_t(tree::OpCode::Push, "1", std::nullopt) == instructions[0]);
    }

  SECTION("false")
    {
      Lexer lexer;
      Parser parser { lexer };
      Env env;
      tree::ThreeAddressCode tac { &env };  
      tree::Node& node = const_cast<tree::Node&>( parser("false;") );
      node.visit(tac);
      std::vector<tree::tac_t> instructions = tac.instructions();
      
      REQUIRE( 1 == instructions.size() );
      REQUIRE(tree::tac_t(tree::OpCode::Push, "0", std::nullopt) == instructions[0]);
    }

  SECTION("arith-and")
    {
      Lexer lexer;
      Parser parser { lexer };
      Env env;
      tree::ThreeAddressCode tac { &env };  
      tree::Node& node = const_cast<tree::Node&>( parser("false && true;") );
      node.visit(tac);
      std::vector<tree::tac_t> instructions = tac.instructions();
      
      REQUIRE( 3 == instructions.size() );
      REQUIRE(tree::tac_t(tree::OpCode::Push, "1", std::nullopt) == instructions[0]);
      REQUIRE(tree::tac_t(tree::OpCode::Push, "0", std::nullopt) == instructions[1]);
      REQUIRE(tree::tac_t(tree::OpCode::And, std::nullopt, std::nullopt) == instructions[2]);
    }

  SECTION("arith-or")
    {
      Lexer lexer;
      Parser parser { lexer };
      Env env;
      tree::ThreeAddressCode tac { &env };  
      tree::Node& node = const_cast<tree::Node&>( parser("true || false;") );
      node.visit(tac);
      std::vector<tree::tac_t> instructions = tac.instructions();
      
      REQUIRE( 3 == instructions.size() );
      REQUIRE(tree::tac_t(tree::OpCode::Push, "0", std::nullopt) == instructions[0]);
      REQUIRE(tree::tac_t(tree::OpCode::Push, "1", std::nullopt) == instructions[1]);
      REQUIRE(tree::tac_t(tree::OpCode::Or, std::nullopt, std::nullopt) == instructions[2]);
    }

  SECTION("arith-not")
    {
      Lexer lexer;
      Parser parser { lexer };
      Env env;
      tree::ThreeAddressCode tac { &env };  
      tree::Node& node = const_cast<tree::Node&>( parser("!false;") );
      node.visit(tac);
      std::vector<tree::tac_t> instructions = tac.instructions();
      
      REQUIRE( 2 == instructions.size() );
      REQUIRE(tree::tac_t(tree::OpCode::Push, "0", std::nullopt) == instructions[0]);
      REQUIRE(tree::tac_t(tree::OpCode::Not, std::nullopt, std::nullopt) == instructions[1]);
    }
}

TEST_CASE("ThreeAddressCode-cmp", "[THREEADDRESSCODE]")
{
  auto test = [](std::string const& source,
		 std::string const& rhs,
		 std::string const& lhs,
		 tree::OpCode op){
		Lexer lexer;
		Parser parser { lexer };
		Env env;
		tree::ThreeAddressCode tac { &env };  
		tree::Node& node = const_cast<tree::Node&>( parser(source) );
		node.visit(tac);
		std::vector<tree::tac_t> instructions = tac.instructions();
      
		REQUIRE( 3 == instructions.size() );
		REQUIRE(tree::tac_t(tree::OpCode::Push, rhs, std::nullopt) == instructions[0]);
		REQUIRE(tree::tac_t(tree::OpCode::Push, lhs, std::nullopt) == instructions[1]);
		REQUIRE(tree::tac_t(op, std::nullopt, std::nullopt) == instructions[2]);
	      };

  test("34 < 25;", "25", "34", tree::OpCode::Lt);
  test("27 <= 32;", "32", "27", tree::OpCode::Le);
  test("153 <== 21;", "21", "153", tree::OpCode::Leq);

  test("134 > 215;", "215", "134", tree::OpCode::Gt);
  test("27 >= 3;", "3", "27", tree::OpCode::Ge);
  test("7 >== 42;", "42", "7", tree::OpCode::Geq);

  test("53 == 32;", "32", "53", tree::OpCode::Equal);
  test("5 != 7;", "7", "5", tree::OpCode::Ne);

  test("53 === 32;", "32", "53", tree::OpCode::Equiv);
  test("5 !== 7;", "7", "5", tree::OpCode::Neq);
}

TEST_CASE("ThreeAddressCode-assert", "[THREEADDRESSCODE]")
{
  SECTION("simple")
    {      
      Lexer lexer;
      Parser parser { lexer };
      Env env;
      tree::ThreeAddressCode tac { &env };  
      tree::Node& node = const_cast<tree::Node&>( parser("_assert false;") );
      node.visit(tac);
      std::vector<tree::tac_t> instructions = tac.instructions();
      
      REQUIRE( 2 == instructions.size() );
      REQUIRE(tree::tac_t(tree::OpCode::Push, "0", std::nullopt) == instructions[0]);
      REQUIRE(tree::tac_t(tree::OpCode::Assert, std::nullopt, std::nullopt) == instructions[1]);
    }

  SECTION("expr")
    {      
      Lexer lexer;
      Parser parser { lexer };
      Env env;
      tree::ThreeAddressCode tac { &env };  
      tree::Node& node = const_cast<tree::Node&>( parser("_assert 5 > 4 && 3 < 12;") );
      node.visit(tac);
      std::vector<tree::tac_t> instructions = tac.instructions();
      
      REQUIRE( 8 == instructions.size() );
      REQUIRE(tree::tac_t(tree::OpCode::Push, "4", std::nullopt) == instructions[0]);
      REQUIRE(tree::tac_t(tree::OpCode::Push, "5", std::nullopt) == instructions[1]);
      REQUIRE(tree::tac_t(tree::OpCode::Gt, std::nullopt, std::nullopt) == instructions[2]);

      REQUIRE(tree::tac_t(tree::OpCode::Push, "12", std::nullopt) == instructions[3]);
      REQUIRE(tree::tac_t(tree::OpCode::Push, "3", std::nullopt) == instructions[4]);
      REQUIRE(tree::tac_t(tree::OpCode::Lt, std::nullopt, std::nullopt) == instructions[5]);

      REQUIRE(tree::tac_t(tree::OpCode::And, std::nullopt, std::nullopt) == instructions[6]);
      
      REQUIRE(tree::tac_t(tree::OpCode::Assert, std::nullopt, std::nullopt) == instructions[7]);

    }
}

TEST_CASE("ThreeAddressCode-dump", "[THREEADDRESSCODE]")
{
  SECTION("simple")
    {      
      Lexer lexer;
      Parser parser { lexer };
      Env env;
      tree::ThreeAddressCode tac { &env };  
      tree::Node& node = const_cast<tree::Node&>( parser("_dump 4;") );
      node.visit(tac);
      std::vector<tree::tac_t> instructions = tac.instructions();
      
      REQUIRE( 2 == instructions.size() );
      REQUIRE(tree::tac_t(tree::OpCode::Push, "4", std::nullopt) == instructions[0]);
      REQUIRE(tree::tac_t(tree::OpCode::Dump, std::nullopt, std::nullopt) == instructions[1]);
    }

  SECTION("expr")
    {      
      Lexer lexer;
      Parser parser { lexer };
      Env env;
      tree::ThreeAddressCode tac { &env };  
      tree::Node& node = const_cast<tree::Node&>( parser("_dump 5 > 4 && 3 < 12;") );
      node.visit(tac);
      std::vector<tree::tac_t> instructions = tac.instructions();
      
      REQUIRE( 8 == instructions.size() );
      REQUIRE(tree::tac_t(tree::OpCode::Push, "4", std::nullopt) == instructions[0]);
      REQUIRE(tree::tac_t(tree::OpCode::Push, "5", std::nullopt) == instructions[1]);
      REQUIRE(tree::tac_t(tree::OpCode::Gt, std::nullopt, std::nullopt) == instructions[2]);

      REQUIRE(tree::tac_t(tree::OpCode::Push, "12", std::nullopt) == instructions[3]);
      REQUIRE(tree::tac_t(tree::OpCode::Push, "3", std::nullopt) == instructions[4]);
      REQUIRE(tree::tac_t(tree::OpCode::Lt, std::nullopt, std::nullopt) == instructions[5]);

      REQUIRE(tree::tac_t(tree::OpCode::And, std::nullopt, std::nullopt) == instructions[6]);
      
      REQUIRE(tree::tac_t(tree::OpCode::Dump, std::nullopt, std::nullopt) == instructions[7]);
    }
}


TEST_CASE("ThreeAddressCode-ident", "[THREEADDRESSCODE]")
{
  SECTION("vardecl")
    {      
      Lexer lexer;
      Parser parser { lexer };
      Env env;
      tree::ThreeAddressCode tac { &env };  
      tree::Node& node = const_cast<tree::Node&>( parser("hello : int = 72;") );
      node.visit(tac);
      std::vector<tree::tac_t> instructions = tac.instructions();
      
      REQUIRE( instructions.empty() );
    }

  SECTION("ident-resolution")
    {      
      Lexer lexer;
      Parser parser { lexer };
      Env env;
      tree::ThreeAddressCode tac { &env };  
      tree::Node& node = const_cast<tree::Node&>( parser("hello : int = 34; _dump hello;") );
      node.visit(tac);
      std::vector<tree::tac_t> instructions = tac.instructions();
      
      REQUIRE( 2 == instructions.size() );
      REQUIRE(tree::tac_t(tree::OpCode::Push, "34", std::nullopt) == instructions[0]);
      REQUIRE(tree::tac_t(tree::OpCode::Dump, std::nullopt, std::nullopt) == instructions[1]);
    }

  SECTION("arith-ident-resolution")
    {      
      Lexer lexer;
      Parser parser { lexer };
      Env env;
      tree::ThreeAddressCode tac { &env };  
      tree::Node& node = const_cast<tree::Node&>( parser("hello : int = 34; world : int = 5; _dump hello + world;") );
      node.visit(tac);
      std::vector<tree::tac_t> instructions = tac.instructions();
      
      REQUIRE( 4 == instructions.size() );
      REQUIRE(tree::tac_t(tree::OpCode::Push, "5", std::nullopt) == instructions[0]);
      REQUIRE(tree::tac_t(tree::OpCode::Push, "34", std::nullopt) == instructions[1]);
      REQUIRE(tree::tac_t(tree::OpCode::Add, std::nullopt, std::nullopt) == instructions[2]);
      REQUIRE(tree::tac_t(tree::OpCode::Dump, std::nullopt, std::nullopt) == instructions[3]);
    }

    SECTION("arith-vardecl-formula")
    {      
      Lexer lexer;
      Parser parser { lexer };
      Env env;
      tree::ThreeAddressCode tac { &env };  
      tree::Node& node = const_cast<tree::Node&>( parser("hello : int = 3 * 7 + 1; _dump hello;") );
      node.visit(tac);
      std::vector<tree::tac_t> instructions = tac.instructions();
      
      REQUIRE( 2 == instructions.size() );
      REQUIRE(tree::tac_t(tree::OpCode::Push, "22", std::nullopt) == instructions[0]);
      REQUIRE(tree::tac_t(tree::OpCode::Dump, std::nullopt, std::nullopt) == instructions[3]);
    }
}

TEST_CASE("ThreeAddressCode-vardecl-bug", "[THREEADDRESSCODE]")
{
  Lexer lexer;
  Parser parser { lexer };
  Env env;

  REQUIRE( !env.contains("hello") );
  tree::ThreeAddressCode tac { &env };  
  tree::Node& node = const_cast<tree::Node&>( parser("hello : int = 72;") );
  node.visit(tac);
  
  REQUIRE( env.contains("hello") );
}

TEST_CASE("ThreeAddressCode-block-bug", "[THREEADDRESSCODE]")
{
  Lexer lexer;
  Parser parser { lexer };
  Env env;
  tree::ThreeAddressCode tac { &env };  
  tree::Node& node = const_cast<tree::Node&>( parser("{x : int = 72; x+1}") );
  node.visit(tac);
  tree::ValueOf vo { &env };
  node.visit(vo);
  
  REQUIRE( "73" == vo() );
}

TEST_CASE("ThreeAddressCode-nested-loop-bug", "[THREEADDRESSCODE]")
{
  Lexer lexer;
  Parser parser { lexer };
  Env env;
  tree::ThreeAddressCode tac { &env };
  std::stringstream source;
  source << "x : int = 0; y : int = 0; counter : int = 0;" << std::endl;
  source << "loop x < 10 { y = 0; loop y < 10 { counter = counter + 1; y = y + 1}; x = x + 1}" << std::endl;
  
  tree::Node& node = const_cast<tree::Node&>( parser(source.str()) );
  node.visit(tac);
  
  REQUIRE( *env.get("x") == "10" );
  REQUIRE( *env.get("counter") == "100" );
}
