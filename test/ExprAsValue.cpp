#include <sstream>
#include <iostream>
#include <catch2/catch.hpp>
#include <ThreeAddressCode.hpp>
#include <TypeController.hpp>
#include <Parser.hpp>
#include <Lexer.hpp>
#include <TreeNodes.hpp>
#include <Env.hpp>
#include <ValueOf.hpp>

TEST_CASE("ExprAsValue", "[EXPRASVALUE]")
{
  auto should_pass = [](std::string const& source, std::string const& val){
		       Lexer lexer;
		       Parser parser { lexer };
		       Env env;  
		       tree::ThreeAddressCode tac { &env };
		       tree::Node& n = const_cast<tree::Node&>( parser(source) );
		       n.visit(tac);
		       tree::TypeController type { &env };
		       n.visit(type);
		       tree::ValueOf value { &env };
		       n.visit(value);  
		       REQUIRE(val == value());
		     };

  SECTION("dump-and-assert")
    {
      should_pass("_dump (_dump 3 * 6);", "18");
      should_pass("_dump (_assert true);", "1");
    }

  SECTION("block")
    {
      should_pass("({1;2;3;8;9})", "9");
      should_pass("({1;2;3;8;7})", "7");
    }

  SECTION("int")
    {
      should_pass("4 + ({300;40;6})", "10");
      should_pass("({300;40;6}) + 5;", "11");
      should_pass("({2; 4; 6}) * 3", "18");
    }
  
  SECTION("vardecl")
    {
      should_pass("x : int = ({0; 1; 2; 63}); x", "63");
    }
}

TEST_CASE("ExprAsValue-assign-bug", "[EXPRASVALUE]")
{
  Lexer lexer;
  Parser parser { lexer };
  Env env;  
  tree::ThreeAddressCode tac { &env };
  tree::Node& n = const_cast<tree::Node&>( parser("x : int = 0; _dump (x = 32)") );
  n.visit(tac);

  Env etc;
  tree::TypeController type { &etc };
  n.visit(type);
  
  Env ev;
  tree::ValueOf value { &ev };  
  n.visit(value);
  
  REQUIRE("32" == value());
}

TEST_CASE("ExprAsValue-if", "[EXPRASVALUE]")
{
  SECTION("int")
    {
      std::stringstream source;

      source << "5 + (if {" << std::endl;
      source << "(1 == 2) false" << std::endl;
      source << "(2 == 2) 42" << std::endl;
      source << "(else) 31" << std::endl;  
      source << "})" << std::endl;
      
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>( parser(source.str()) );
      Env ev;
      tree::ValueOf value { &ev };  
      n.visit(value);  
      REQUIRE("47" == value());
    }

  SECTION("bool")
    {
      std::stringstream source;

      source << "true || (if {" << std::endl;
      source << "(2 == 2) false" << std::endl;
      source << "(2 == 2) 42" << std::endl;
      source << "(else) 31" << std::endl;  
      source << "})" << std::endl;
      
      Lexer lexer;
      Parser parser { lexer };
      tree::Node& n = const_cast<tree::Node&>( parser(source.str()) );
      Env ev;
      tree::ValueOf value { &ev };  
      n.visit(value);  
      REQUIRE("1" == value());
    }
}

TEST_CASE("ExprAsValue-loop", "[EXPRASVALUE]")
{
  SECTION("simple")
    {
      // TODO
     //  std::stringstream source;

    //   source << "5 + (loop" << std::endl;
    //   source << "{" << std::endl;
    //   source << "" << std::endl;
    //   source << "" << std::endl;
    //   source << "}" << std::endl;
      
    //   Lexer lexer;
    //   Parser parser { lexer };
    //   tree::Node& n = const_cast<tree::Node&>( parser(source.str()) );
    //   Env ev;
    //   tree::ValueOf value { &ev };  
    //   n.visit(value);  
    //   REQUIRE("47" == value());
    }
}
