#include <TreeNodes.hpp>
#include <AstVisitor.hpp>

namespace tree
{
#define GEN_VISITOR(NAME) void NAME::visit(AstVisitor& visitor) { visitor.accept(*this); }
  
  /* virtual */ bool Node::operator==(Node const& node) const
  {      
    return name == node.name
      && ((m == nullptr || node.m == nullptr) ? (m == nullptr && node.m == nullptr) : (*m == *node.m))
      && ((n == nullptr || node.n == nullptr) ? (n == nullptr && node.n == nullptr) : (*n == *node.n));
  }

  /* virtual */ bool Node::operator!=(Node const& node) const
  {
    return !(*this == node);
  }

  /* virtual */ std::string Node::to_string() const
  {
    return name +
      "(" +
      (m ? m->to_string() : "") +
      ", " +
      (n ? n->to_string() : "") +
      ")";
  }
	
  std::ostream& operator<<(std::ostream& os, Node const& node)
  {
    os << node.name << " ("
       << (node.m ? node.m->to_string() : "nullptr")
       << ", "
       << (node.n ? node.n->to_string() : "nullptr")
       << ")";
    
    return os;
  }
  
  /*explicit*/ IntValue::IntValue(int val) : value { val }
  {
    name = "IntValue";
  }
  
  /*virtual*/ bool IntValue::operator==(Node const& node) const /*override*/
  {
    bool same_value = true;

    if (node.name == name)
      {
	same_value = value == static_cast<const IntValue*>(&node)->value;
      }
      
    return name == node.name
      && ((m == nullptr || node.m == nullptr) ? (m == nullptr && node.m == nullptr) : (*m == *node.m))
      && ((n == nullptr || node.n == nullptr) ? (n == nullptr && node.n == nullptr) : (*n == *node.n))
      && same_value;
  }

  /*explicit*/ StringValue::StringValue(std::string const& val) : value { val }
  {
    name = "StringValue";
  }
  
  /*virtual*/ bool StringValue::operator==(Node const& node) const /*override*/
  {
    bool same_value = true;

    if (node.name == name)
      {
	same_value = value == static_cast<const StringValue*>(&node)->value;
      }
      
    return name == node.name
      && ((m == nullptr || node.m == nullptr) ? (m == nullptr && node.m == nullptr) : (*m == *node.m))
      && ((n == nullptr || node.n == nullptr) ? (n == nullptr && node.n == nullptr) : (*n == *node.n))
      && same_value;
  }

  /*explicit*/ BoolValue::BoolValue(bool val) : value { val }
  {
    name = "BoolValue";
  }

  /*virtual*/ bool BoolValue::operator==(Node const& node) const /*override*/
  {
    bool same_value = true;

    if (node.name == name)
      {
	same_value = value == static_cast<const BoolValue*>(&node)->value;
      }
      
    return name == node.name
      && ((m == nullptr || node.m == nullptr) ? (m == nullptr && node.m == nullptr) : (*m == *node.m))
      && ((n == nullptr || node.n == nullptr) ? (n == nullptr && node.n == nullptr) : (*n == *node.n))
      && same_value;
  }

  /*explicit*/ Ident::Ident(std::string const& ident)
  {
    name = "Ident";
    m = std::make_unique<StringValue>(ident);
    n = nullptr;
  }
  
  /*virtual*/ std::string Ident::to_string() const
  {
    return name + "(" + static_cast<StringValue*>(m.get())->value + ")";
  }

  /*virtual*/ std::string Ident::value() const
  {
    return static_cast<StringValue*>(m.get())->value;
  }
  
  /*explicit*/ Int::Int(int val)
  {
    name = "Int";
    m = std::make_unique<IntValue>(val);
    n = nullptr;
  }

  /*virtual*/ std::string Int::to_string() const /*override*/
  {
    std::stringstream ss;
    ss << name << "(" << static_cast<IntValue*>(m.get())->value << ")";
    return ss.str();
  }

  /*explicit*/ Bool::Bool(bool val)
  {
    name = "Bool";
    m = std::make_unique<BoolValue>(val);
    n = nullptr;
  }

  /*virtual*/ std::string Bool::to_string() const /*override*/
  {
    std::stringstream ss;
    ss << name << "(" << std::boolalpha << static_cast<BoolValue*>(m.get())->value << ")";
    return ss.str();
  }

  GEN_VISITOR(VarDecl)
  GEN_VISITOR(Ident)
  GEN_VISITOR(StringValue)
  GEN_VISITOR(Seq)
  GEN_VISITOR(IntValue)
  GEN_VISITOR(Int)
  GEN_VISITOR(BoolValue)
  GEN_VISITOR(Bool)

  GEN_VISITOR(Add);
  GEN_VISITOR(Sub);
  GEN_VISITOR(Mul);
  GEN_VISITOR(Div);
  GEN_VISITOR(Mod);

  GEN_VISITOR(And);
  GEN_VISITOR(Or);
  GEN_VISITOR(Not);

  GEN_VISITOR(Lt);
  GEN_VISITOR(Le);
  GEN_VISITOR(Leq);
  GEN_VISITOR(Gt);
  GEN_VISITOR(Ge);
  GEN_VISITOR(Geq);
  GEN_VISITOR(Equal);
  GEN_VISITOR(Equiv);
  GEN_VISITOR(Ne);
  GEN_VISITOR(Neq);

  GEN_VISITOR(Block);
  GEN_VISITOR(Assert);
  GEN_VISITOR(Dump);
  GEN_VISITOR(Usub);
  GEN_VISITOR(Assign);
  
  GEN_VISITOR(If);
  GEN_VISITOR(IfEntry);
  GEN_VISITOR(Loop);
  GEN_VISITOR(Array);
}
