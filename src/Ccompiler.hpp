#ifndef CCOMPILER_HPP
#define CCOMPILER_HPP
#include <std.pch.hpp>

namespace tree { class ThreeAddressCode; }

class Ccompiler
{
public:
  explicit Ccompiler(tree::ThreeAddressCode& tac);

  std::string header();
  std::string source();
  std::string footer();
  
  virtual ~Ccompiler();

private:
  std::stringstream m_source;
  tree::ThreeAddressCode& m_tac;
  
  Ccompiler(Ccompiler const& ccompiler) = delete;
  Ccompiler& operator=(Ccompiler const& ccompiler) = delete;
};

#endif // CCOMPILER
