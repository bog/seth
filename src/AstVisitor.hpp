
#ifndef ASTVISITOR_HPP
#define ASTVISITOR_HPP
#include <TreeNodes.hpp>

namespace tree
{  
  class AstVisitor
  {
  public:
    AstVisitor();
    virtual void accept(Array& node) = 0;
    virtual void accept(Loop& node) = 0;
    virtual void accept(IfEntry& node) = 0;
    virtual void accept(If& node) = 0;
    virtual void accept(Assign& node) = 0;
    virtual void accept(Block& node) = 0;
    virtual void accept(Ident& node) = 0;
    virtual void accept(StringValue& node) = 0;
    virtual void accept(VarDecl& node) = 0;
    virtual void accept(Seq& node) = 0;
    virtual void accept(IntValue& node) = 0;
    virtual void accept(Int& node) = 0;
    virtual void accept(BoolValue& node) = 0;
    virtual void accept(Bool& node) = 0;
    virtual void accept(Add& node) = 0;
    virtual void accept(Sub& node) = 0;
    virtual void accept(Mul& node) = 0;
    virtual void accept(Div& node) = 0;
    virtual void accept(Mod& node) = 0;
    virtual void accept(And& node) = 0;
    virtual void accept(Or& node) = 0;
    virtual void accept(Not& node) = 0;
    virtual void accept(Lt& node) = 0;
    virtual void accept(Le& node) = 0;
    virtual void accept(Leq& node) = 0;
    virtual void accept(Gt& node) = 0;
    virtual void accept(Ge& node) = 0;
    virtual void accept(Geq& node) = 0;
    virtual void accept(Equal& node) = 0;
    virtual void accept(Equiv& node) = 0;
    virtual void accept(Ne& node) = 0;
    virtual void accept(Neq& node) = 0;
    virtual void accept(Assert& node) = 0;
    virtual void accept(Dump& node) = 0;
    virtual void accept(Usub& node) = 0;

    virtual ~AstVisitor();
  private:
    AstVisitor(AstVisitor const& astvisitor) = delete;
    AstVisitor& operator=(AstVisitor const& astvisitor) = delete;
  };
}
#endif // ASTVISITOR
