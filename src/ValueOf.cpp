#include <iostream>
#include <ValueOf.hpp>
#include <Env.hpp>

namespace tree
{
#define VALUE_OF_BINARY(OP) node.m->visit(*this);			\
  node.n->visit(*this);							\
									\
  std::string t1 = m_stack.back();					\
  m_stack.pop_back();							\
									\
  std::string t0 = m_stack.back();					\
  m_stack.pop_back();							\
									\
  m_stack.push_back(std::to_string(atoi(t0.c_str()) OP atoi(t1.c_str()))) \

#define VALUE_OF_UNARY(OP) node.m->visit(*this);	\
  std::string t = m_stack.back();			\
  m_stack.pop_back();					\
  m_stack.push_back(std::to_string(OP atoi(t.c_str())))	\

  ValueOf::ValueOf(Env* env)
  : m_env { env }
  {
  }

  std::string ValueOf::operator()() const
  {
    if (m_stack.empty())
      {
	return "";
      }
    
    return m_stack.back();
  }

  /*virtual*/ void ValueOf::accept(Array& node)
  {
    m_stack.push_back(node.to_string());
  }

  /*virtual*/ void ValueOf::accept(Loop& node)
  {
    std::cerr<< "TODO ValueOf Loop" <<std::endl;
    exit(-1);
  }
  
  /*virtual*/ void ValueOf::accept(If& node)
  {
    size_t stack_sz = m_stack.size();

    m_if_matched = false;
    
    node.m->visit(*this);

    if (stack_sz == m_stack.size())
      {
	m_stack.push_back("0");
      }
  }

  /*virtual*/ void ValueOf::accept(IfEntry& node)
  {
    if (node.m)
      {
	node.m->visit(*this);
	std::string value = m_stack.back();
	m_stack.pop_back();

	if (value == "1" && !m_if_matched)
	  {
	    m_if_matched = true;
	    node.n->visit(*this);
	  }
      }
    else if(!m_if_matched)
      {
	m_if_matched = true;
	node.n->visit(*this);
      }
    
  }

  /*virtual*/ void ValueOf::accept(Assign& node)
  {
    std::string name = static_cast<tree::Ident*>(node.m.get())->value();
    node.n->visit(*this);
    
    std::string value = m_stack.back();
    m_stack.pop_back();
    
    m_env->set(name, value);

    m_stack.push_back(value);
  }
  
  /*virtual*/ void ValueOf::accept(Block& node)
  {
    m_env->block();

    std::vector<std::string> stack = m_stack;    
    node.m->visit(*this);
    
    stack.push_back(m_stack.back());
    m_stack = stack;
    
    m_env->unblock();
  }

  /*virtual*/ void ValueOf::accept(Ident& node)
  {
    node.m->visit(*this);
    
    std::string name = m_stack.back();
    m_stack.pop_back();

    if ( m_env->contains(name) )
      {
	m_stack.push_back(*m_env->get(name));
      }
    else
      {
	m_stack.push_back("0");
	MsgStatus::error(name + " is not declared");
      }
  }

  /*virtual*/ void ValueOf::accept(StringValue& node)
  {
    m_stack.push_back(node.value);
  }

  /*virtual*/ void ValueOf::accept(VarDecl& node)
  {
    std::string name = static_cast<tree::Ident*>(node.m.get())->value();
    node.n->visit(*this);

    std::string value = m_stack.back();
    m_stack.pop_back();
    m_env->declare(name, node.type, value);
  }

  /*virtual*/ void ValueOf::accept(Seq& node)
  {
    if (node.m)
      {
	node.m->visit(*this);
      }

    if (node.n)
      {
	node.n->visit(*this);
      }
  }

  /*virtual*/ void ValueOf::accept(IntValue& node)
  {
    m_stack.push_back(std::to_string(node.value));
  }

  /*virtual*/ void ValueOf::accept(Int& node)
  {
    node.m->visit(*this);
  }

  /*virtual*/ void ValueOf::accept(BoolValue& node)
  {
    m_stack.push_back(std::to_string(node.value));
  }

  /*virtual*/ void ValueOf::accept(Bool& node)
  {
    node.m->visit(*this);
  }

  /*virtual*/ void ValueOf::accept(Add& node)
  {
    VALUE_OF_BINARY(+);
  }

  /*virtual*/ void ValueOf::accept(Sub& node)
  {
    VALUE_OF_BINARY(-);
  }

  /*virtual*/ void ValueOf::accept(Mul& node)
  {
    VALUE_OF_BINARY(*);
  }

  /*virtual*/ void ValueOf::accept(Div& node)
  {
    VALUE_OF_BINARY(/);
  }

  /*virtual*/ void ValueOf::accept(Mod& node)
  {
    VALUE_OF_BINARY(%);
  }

  /*virtual*/ void ValueOf::accept(And& node)
  {
    VALUE_OF_BINARY(&&);
  }

  /*virtual*/ void ValueOf::accept(Or& node)
  {
    VALUE_OF_BINARY(||);
  }

  /*virtual*/ void ValueOf::accept(Not& node)
  {
    VALUE_OF_UNARY(!);
  }

  /*virtual*/ void ValueOf::accept(Lt& node)
  {
    VALUE_OF_BINARY(<);
  }

  /*virtual*/ void ValueOf::accept(Le& node)
  {
    VALUE_OF_BINARY(<=);
  }

  /*virtual*/ void ValueOf::accept(Leq& node)
  {
    VALUE_OF_BINARY(<=);
  }

  /*virtual*/ void ValueOf::accept(Gt& node)
  {
    VALUE_OF_BINARY(>);
  }

  /*virtual*/ void ValueOf::accept(Ge& node)
  {
    VALUE_OF_BINARY(>=);
  }

  /*virtual*/ void ValueOf::accept(Geq& node)
  {
    VALUE_OF_BINARY(>=);
  }

  /*virtual*/ void ValueOf::accept(Equal& node)
  {
    VALUE_OF_BINARY(==);
  }

  /*virtual*/ void ValueOf::accept(Equiv& node)
  {
    VALUE_OF_BINARY(==);
  }

  /*virtual*/ void ValueOf::accept(Ne& node)
  {
    VALUE_OF_BINARY(!=);
  }

  /*virtual*/ void ValueOf::accept(Neq& node)
  {
    VALUE_OF_BINARY(!=);
  }

  /*virtual*/ void ValueOf::accept(Assert& node)
  {
    node.m->visit(*this);

    std::string t = m_stack.back();
    m_stack.pop_back();

    m_stack.push_back(t == "0" ? "0" : "1");
  }

  /*virtual*/ void ValueOf::accept(Dump& node)
  {
    node.m->visit(*this);
  }

  /*virtual*/ void ValueOf::accept(Usub& node)
  {
    VALUE_OF_UNARY(-);
  }

  ValueOf::~ValueOf()
  {
  }
}
