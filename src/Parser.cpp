#include <iostream>
#include <Parser.hpp>
#include <TreeNodes.hpp>
#include <Lexer.hpp>
#include <MsgStatus.hpp>

Parser::Parser(Lexer const& lexer)
  : m_lexer { lexer }
  , m_current { 0 }
  , m_result { nullptr }
{
}

Parser::~Parser()
{
  if (m_result)
    {
      delete m_result;
      m_result = nullptr;
    }
}

tree::Node const& Parser::operator()(std::string const& input) const
{
  std::vector<Token> tokens = m_lexer(input);
  m_current = 0;

  if (m_result)
    {
      delete m_result;
    }
  
  m_result = instr(tokens);

  if (m_result != nullptr)
    {
      return *m_result;
    }

  std::cerr<< "Error" <<std::endl;
  exit(-1);
}

bool Parser::match(std::vector<Token> const& tokens, std::vector<TokenType> const& toktypes) const
{
  int offset = 0;

  for (size_t i=0; i<toktypes.size(); i++)
    {
      if (m_current + offset < tokens.size() && tokens[m_current + offset].type == toktypes[i])
	{
	  offset++;
	}
      else
	{
	  return false;
	}
    }

  m_current += offset;
  return true;
}

bool Parser::match(std::vector<Token> const& tokens, TokenType const& toktype) const
{
  if (m_current < tokens.size() &&
      tokens[m_current].type == toktype)
    {
      if (m_current < tokens.size())
	{
	  m_current++;
	}
      
      return true;
    }
  
  return false;
}

bool Parser::at_end(std::vector<Token> const& tokens) const
{
  return m_current >= tokens.size();
}

/*
  INSTR   -> EXPR (SEMICOLON EXPR)*
*/
tree::Node* Parser::instr(std::vector<Token> const& tokens) const
{  
  tree::Node* n =  expr(tokens);
  
  while ( match(tokens, TokenType::SemiColon) )
    {
      if ( at_end(tokens) )
	{
	  return n;
	}
      else
	{	 
	  n = new tree::Seq(n, expr(tokens));
	}
    }

  return n;
}

/*
  EXPR    -> ANDOR | _dump ANDOR | _assert ANDOR
  | ident COLON TYPE ASSIGN ANDOR  | OpenBlock INSTR CloseBlock
  | if OpenBlock (IFENTRY)* CloseBlock
  | loop expr OpenBlock INSTR CloseBlock
*/
tree::Node* Parser::expr(std::vector<Token> const& tokens) const
{
  size_t current = m_current;
  
  // loop
  if ( match(tokens, TokenType::Loop) )
    {
      tree::Node* cond = expr(tokens);
      if ( match(tokens, TokenType::OpenBlock) )
	{
	  tree::Node* body = instr(tokens);
	  
	  if ( match(tokens, TokenType::CloseBlock) )
	    {
	      return new tree::Loop(cond, body);
	    }
	  else
	    {
	      MsgStatus::error("missing ) in loop");
	      m_current = current;
	    }
	}
      else
	{
	  MsgStatus::error("expected { in loop");
	  m_current = current;
	}
    }
  
  // if
  if ( match(tokens, {TokenType::If, TokenType::OpenBlock}) )
    {
      tree::Node* entry;
      tree::Node* body;

      if ( (entry = ifentry(tokens)) )
      	{
      	  body = entry;
      	}

      while ( (entry = ifentry(tokens)) )
      	{
      	  body = new tree::Seq(body, entry);
      	}
      
      if ( match(tokens, TokenType::CloseBlock) )
	{
	  return new tree::If(body);
	}
      else
	{
	  MsgStatus::error("expecting } in if expression");
	  return new tree::Int(0);
	}
    }
  
  // Valid vardecl
  if ( match(tokens, {
		      TokenType::Ident,
		      TokenType::Colon,
		      TokenType::Type,
		      TokenType::Assign}))
    {
      std::string name = tokens[m_current-4].inspect;
      std::string type_str = tokens[m_current-2].inspect;
      tree::Node* value = andor(tokens);
      
      Type type;      
      if (type_str == "int") { type = Type::Int; }
      else if (type_str == "bool") { type = Type::Bool; }
      else if (type_str == "array") { type = Type::Array; }

      return new tree::VarDecl(new tree::Ident(name), type, value);      
    }

  // VarDecl bad type
  if ( match(tokens, {
		      TokenType::Ident,
		      TokenType::Colon,
		      TokenType::Ident,
		      TokenType::Assign}))
    {
      std::string name = tokens[m_current-4].inspect;
      std::string type_str = tokens[m_current-2].inspect;

      tree::Node* value = andor(tokens);
      MsgStatus::error(type_str + " does not name a valid type.");
      return new tree::VarDecl(new tree::Ident(name), Type::Invalid, value);      
    }

  // Assignment
  if ( match(tokens, {
		      TokenType::Ident,
		      TokenType::Assign}))
    {
      std::string name = tokens[m_current-2].inspect;
      tree::Node* value = andor(tokens);
      
      return new tree::Assign(new tree::Ident(name), value);      
    }
    
  if ( match(tokens, TokenType::Dump) )
    {
      tree::Node* n = andor(tokens);
      return new tree::Dump(n);      
    }

  if ( match(tokens, TokenType::Assert) )
    {
      tree::Node* n = andor(tokens);
      return new tree::Assert(n);
    }

  if ( match(tokens, TokenType::OpenBlock) )
    {
      tree::Node* n = instr(tokens);
      
      if ( match(tokens, TokenType::CloseBlock) )
	{
	  return new tree::Block(n);
	}
      else
	{
	  MsgStatus::error("Missing block }");
	}
    }

  return andor(tokens);
}

// IFENTRY -> OpenPar (else | expr) ClosePar expr
tree::Node* Parser::ifentry(std::vector<Token> const& tokens) const
{
  tree::Node* cond;
  tree::Node* res;

  if ( match(tokens, TokenType::OpenPar ) )
    {
      if ( match(tokens, TokenType::Else) )
	{
	  cond = nullptr;
	}
      else
	{
	  cond = expr(tokens);
	}
      
      if ( match(tokens, TokenType::ClosePar ) )
	{
	  res = expr(tokens);

	  return new tree::IfEntry(cond, res);
	}
    }

  return nullptr;
}

/*
  ANDOR  -> CMP (&& CMP)* | CMP (|| CMP)*
*/
tree::Node* Parser::andor(std::vector<Token> const& tokens) const
{
  tree::Node* n = cmp(tokens);

  while (true)
    {
      if ( match(tokens, TokenType::And) )
	{
	  n = new tree::And(n, cmp(tokens));
	}

      else if ( match(tokens, TokenType::Or) )
	{
	  n = new tree::Or(n, cmp(tokens));
	}

      else
	{
	  break;
	}
    }

  return n;
}

/*
  CMP -> ADDSUB (CMPSYMB ADDSUB)?
*/
tree::Node* Parser::cmp(std::vector<Token> const& tokens) const
{
  tree::Node* n = addsub(tokens);

  if ( match(tokens, TokenType::Lt) )
    {
      return new tree::Lt(n, addsub(tokens));
    }

  if (match(tokens, TokenType::Le))
    {
      return new tree::Le(n, addsub(tokens));
    }

  if (match(tokens, TokenType::Leq))
    {
      return new tree::Leq(n, addsub(tokens));
    }
    
  if (match(tokens, TokenType::Gt))
    {
      return new tree::Gt(n, addsub(tokens));
    }

  if (match(tokens, TokenType::Ge))
    {
      return new tree::Ge(n, addsub(tokens));
    }

  if (match(tokens, TokenType::Geq))
    {
      return new tree::Geq(n, addsub(tokens));
    }
       
  if (match(tokens, TokenType::Equal))
    {
      return new tree::Equal(n, addsub(tokens));
    }
  
  if (match(tokens, TokenType::Equiv))
    {
      return new tree::Equiv(n, addsub(tokens));
    }

  if (match(tokens, TokenType::Ne))
    {
      return new tree::Ne(n, addsub(tokens));
    }

  if (match(tokens, TokenType::Neq))
    {
      return new tree::Neq(n, addsub(tokens));
    }

  return n;
}

/*
  ADDSUB -> MOD (+ MOD)* | MOD (- MOD)*  
*/
tree::Node* Parser::addsub(std::vector<Token> const& tokens) const
{
  tree::Node* n = mod(tokens);

  while (true)
    {
      if ( match(tokens, TokenType::Add) )
	{
	  n = new tree::Add(n, mod(tokens));
	}
      else if ( match(tokens, TokenType::Sub) )
	{
	  n = new tree::Sub(n, mod(tokens));
	}
      else
	{
	  break;
	}
    }

  return n;
}

/*
  MOD -> MULDIV (% MULDIV)?
*/
tree::Node* Parser::mod(std::vector<Token> const& tokens) const
{
  tree::Node* n = muldiv(tokens);

  if ( match(tokens, TokenType::Mod) )
    {
      n = new tree::Mod(n, muldiv(tokens));
    }
  
  return n;
}

/*
  MULDIV  -> UNARY (* UNARY)* | UNARY (/ UNARY)*
*/
tree::Node* Parser::muldiv(std::vector<Token> const& tokens) const
{
  tree::Node* n = unary(tokens);

  while ( match(tokens, TokenType::Mul) )
    {
      n = new tree::Mul(n, unary(tokens));
    }

  while ( match(tokens, TokenType::Div) )
    {
      n = new tree::Div(n, unary(tokens));
    }
    
  return n;
}

/*
  UNARY  -> -LITERAL | !LITERAL | LITERAL
*/
tree::Node* Parser::unary(std::vector<Token> const& tokens) const
{
  if ( match(tokens, TokenType::Sub) )
    {
      return new tree::Usub(literal(tokens));
    }
  
  if ( match(tokens, TokenType::Not) )
    {
      return new tree::Not(literal(tokens));
    }

  return literal(tokens);
}

/*
  LITERAL -> n | true | false | (EXPR)
               | OpenArray (expr (COMMA expr)*)? CloseArray
*/
tree::Node* Parser::literal(std::vector<Token> const& tokens) const
{
  // array
  if ( match(tokens, TokenType::OpenArray) )
    {
      if ( match(tokens, TokenType::CloseArray) )
	{
	  return new tree::Array(nullptr);
	}
      
      tree::Node* n = expr(tokens);

      while ( match(tokens, TokenType::Comma) )
	{
	  n = new tree::Seq(n, expr(tokens));
	}

      if ( match(tokens, TokenType::CloseArray) )
	{
	  return new tree::Array(n);
	}
      else
	{
	  MsgStatus::error("missing ] in array");
	}
    }

  if ( match(tokens, TokenType::Int) )
    {
      return new tree::Int(tokens[m_current - 1].value.i32);
    }
    
  if ( match(tokens, TokenType::True) )
    {
      return new tree::Bool(true);
    }

  if ( match(tokens, TokenType::False) )
    {
      return new tree::Bool(false);
    }

  if ( match(tokens, TokenType::OpenPar) )
    {
      tree::Node* n = expr(tokens);

      if ( !match(tokens, TokenType::ClosePar) )
	{
	  MsgStatus::error("ClosePar : missing close parenthesis");
	}

      return n;
    }

  if ( match(tokens, TokenType::Ident) )
    {
      return new tree::Ident(tokens[m_current - 1].inspect);
    }

  MsgStatus::error("Unexpected token " +
		   tokens[m_current].inspect +
		   " line " +
		   std::to_string(tokens[m_current].line));
  
  return nullptr;
}
