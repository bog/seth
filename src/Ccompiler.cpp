#include <Ccompiler.hpp>
#include <ThreeAddressCode.hpp>

#define BINARY_C(NAME,OP) h << "void "<< NAME << "() {";	\
  h << "int lhs = atoi(stack[sp-1]);";				\
  h << "int rhs = atoi(stack[sp-2]);";				\
  h << "Pop(); Pop();";						\
  h << "char res[STRING_LIMIT];";				\
  h << "sprintf(res, \"%d\", lhs " OP " rhs);";			\
  h << "Push(res);";						\
  h << "}" << std::endl;					\

#define UNARY_C(NAME,OP) h << "void "<< NAME << "() {";	\
  h << "int lhs = atoi(stack[sp-1]);";			\
  h << "Pop();";					\
  h << "char res[STRING_LIMIT];";			\
  h << "sprintf(res, \"%d\"," OP " lhs);";		\
  h << "Push(res);";					\
  h << "}" << std::endl;				\

Ccompiler::Ccompiler(tree::ThreeAddressCode& tac)
  : m_tac { tac }
{
}

std::string Ccompiler::header()
{
  std::stringstream h;

  h << "#include <stdio.h>" << std::endl;
  h << "#include <stdlib.h>" << std::endl;
  h << "#include <string.h>" << std::endl;
  h << "#define STRING_LIMIT 256" << std::endl;
  h << "#define STACK_LIMIT 256" << std::endl;
  h << "char* stack[STACK_LIMIT];" << std::endl;
  h << "int sp = 0;" << std::endl;

  h << "void Pop() {";
  h << "sp--; free(stack[sp]);";
  h << "}" << std::endl;

  h << "void Push(char const* value) {";
  h << "stack[sp++] = strdup(value);";
  h << "}" << std::endl;

  UNARY_C("Usub", "-");
  BINARY_C("And", "&&");
  BINARY_C("Or", "||");
  UNARY_C("Not", "!");

  BINARY_C("Lt", "<");
  BINARY_C("Le", "<=");
  BINARY_C("Leq", "<=");

  BINARY_C("Gt", ">");
  BINARY_C("Ge", ">=");
  BINARY_C("Geq", ">=");

  BINARY_C("Equal", "==");
  BINARY_C("Equiv", "==");
  BINARY_C("Ne", "!=");
  BINARY_C("Neq", "!=");

  h << "void Assert() {";
  h << "int lhs = atoi(stack[sp-1]); Pop();";
  h << "if (lhs == 0){fprintf(stderr, \"Assertion failed.\\n\"); exit(-1);}";
  h << "char res[STRING_LIMIT];";				\
  h << "sprintf(res, \"%d\", lhs);";			\
  h << "Push(res);";						\
  h << "}" << std::endl;

  h << "void Dump() {";
  h << "int lhs = atoi(stack[sp-1]); Pop();";
  h << "printf(\"%d\\n\", lhs);";
  h << "char res[STRING_LIMIT];";				\
  h << "sprintf(res, \"%d\", lhs);";			\
  h << "Push(res);";						\
  h << "}" << std::endl;
  
  BINARY_C("Add", "+");
  BINARY_C("Sub", "-");
  BINARY_C("Mul", "*");
  BINARY_C("Div", "/");
  BINARY_C("Mod", "%");
  
  h << "int main() {" << std::endl;
  
  return h.str();
}

std::string Ccompiler::source()
{
  std::vector<tree::tac_t> instructions = m_tac.instructions();

  for (auto const& instr : instructions)
    {
      m_source << tree::opcode_str(instr.op);
      m_source << "(";
      
      if (instr.lhs)
	{
	  m_source << "\"" << *instr.lhs << "\"";
	}

      if (instr.rhs)
	{
	  if (instr.lhs) {m_source << ", "; }
	  m_source << "\"" << *instr.rhs << "\"";
	}

      m_source << ");" << std::endl;     
    }

  return m_source.str();
}

std::string Ccompiler::footer()
{
  std::stringstream f;
  f << "while(sp >= 0) { Pop(); }" << std::endl;
  f << "return 0;" << std::endl;
  f << "}" << std::endl;
  
  return f.str();
}

Ccompiler::~Ccompiler()
{
}

