#include <MsgStatus.hpp>

std::vector<std::string> MsgStatus::m_errors;

void MsgStatus::init()
{
  MsgStatus::m_errors.clear();
}

std::vector<std::string> MsgStatus::errors_str()
{
  return MsgStatus::m_errors;
}

void MsgStatus::error(std::string const& msg)
{
  MsgStatus::m_errors.push_back(msg);
}
