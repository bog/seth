#ifndef TYPECONTROLLER_HPP
#define TYPECONTROLLER_HPP
#include <std.pch.hpp>
#include <AstVisitor.hpp>

enum class Type;
class Env;

namespace tree
{
  class TypeController : public AstVisitor
  {
  public:
    TypeController(Env* env);

    bool error();
    virtual void accept(Array& node) override;
    virtual void accept(Loop& node) override;
    virtual void accept(If& node) override;
    virtual void accept(IfEntry& node) override;
    virtual void accept(Assign& node) override;
    virtual void accept(Block& node) override;
    virtual void accept(Ident& node) override;
    virtual void accept(StringValue& node) override;
    virtual void accept(VarDecl& node) override;
    virtual void accept(Seq& node) override;
    virtual void accept(IntValue& node) override;
    virtual void accept(Int& node) override;
    virtual void accept(BoolValue& node) override;
    virtual void accept(Bool& node) override;
    virtual void accept(Add& node) override;
    virtual void accept(Sub& node) override;
    virtual void accept(Mul& node) override;
    virtual void accept(Div& node) override;
    virtual void accept(Mod& node) override;
    virtual void accept(And& node) override;
    virtual void accept(Or& node) override;
    virtual void accept(Not& node) override;
    virtual void accept(Lt& node) override;
    virtual void accept(Le& node) override;
    virtual void accept(Leq& node) override;
    virtual void accept(Gt& node) override;
    virtual void accept(Ge& node) override;
    virtual void accept(Geq& node) override;
    virtual void accept(Equal& node) override;
    virtual void accept(Equiv& node) override;
    virtual void accept(Ne& node) override;
    virtual void accept(Neq& node) override;
    virtual void accept(Assert& node) override;
    virtual void accept(Dump& node) override;
    virtual void accept(Usub& node) override;

    virtual ~TypeController();
    
  private:
    Env* m_env;
    bool m_err = false;
    std::vector<Type> m_stack;
    bool m_if_matched;
    
    bool binary_type(Node& node, Type type);
    bool unary_type(Node& node, Type type);
    
    TypeController(TypeController const& typecontroller) = delete;
    TypeController& operator=(TypeController const& typecontroller) = delete;
  };
}
#endif // TYPECONTROLLER
