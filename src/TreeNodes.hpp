#ifndef TREENODES_HPP
#define TREENODES_HPP
#include <std.pch.hpp>
#include <MsgStatus.hpp>
#include <Type.hpp>

namespace tree
{
  class AstVisitor;
  
  struct Node
  {
    std::string name;
    std::unique_ptr<Node> m;
    std::unique_ptr<Node> n;
    friend std::ostream& operator<<(std::ostream& os, Node const& node);    
    virtual bool operator==(Node const& node) const;
    virtual bool operator!=(Node const& node) const;
    virtual std::string to_string() const;
    virtual void visit(AstVisitor& visitor) = 0;
    virtual ~Node() {}
  };
  
  struct StringValue : public Node
  {
    explicit StringValue(std::string const& val);
    virtual bool operator==(Node const& node) const override;
    virtual void visit(AstVisitor& visitor) override;
    std::string value;    
  };
  
  struct IntValue : public Node
  {
    explicit IntValue(int val);
    virtual bool operator==(Node const& node) const override;
    virtual void visit(AstVisitor& visitor) override;
    int value;    
  };

  struct BoolValue : public Node
  {
    explicit BoolValue(bool val);
    virtual bool operator==(Node const& node) const override;
    virtual void visit(AstVisitor& visitor) override;
    bool value;    
  };

  struct Ident : public Node
  {
    explicit Ident(std::string const& ident);
    virtual std::string to_string() const override;
    virtual void visit(AstVisitor& visitor) override;
    virtual std::string value() const;
  };
  
  struct Int : public Node
  {
    explicit Int(int val);
    virtual std::string to_string() const override;
    virtual void visit(AstVisitor& visitor) override;
  };

  struct Bool : public Node
  {
    explicit Bool(bool val);
    virtual std::string to_string() const override;
    virtual void visit(AstVisitor& visitor) override;
  };

#define TERNARY_RULE(NAME) struct NAME : public Node		\
  {explicit NAME (Node* x, Type t, Node* y){name = #NAME;	\
      m = std::unique_ptr<Node>(x);				\
      n = std::unique_ptr<Node>(y);				\
      type = t;							\
      if (m == nullptr || n == nullptr)				\
	{							\
	  MsgStatus::error(#NAME " : missing arguments");	\
	}							\
    }								\
    virtual ~NAME() {}						\
    virtual void visit(AstVisitor& visitor) override;		\
    Type type;							\
  }								\

  #define OPTIONAL_BINARY_RULE(NAME) struct NAME : public Node		\
  {explicit NAME (Node* lhs, Node* rhs){name = #NAME;		\
      m = std::unique_ptr<Node>(lhs);				\
      n = std::unique_ptr<Node>(rhs);				\
    }								\
    virtual ~NAME() {}						\
    virtual void visit(AstVisitor& visitor) override;		\
  }								\

#define BINARY_RULE(NAME) struct NAME : public Node		\
  {explicit NAME (Node* lhs, Node* rhs){name = #NAME;		\
      m = std::unique_ptr<Node>(lhs);				\
      n = std::unique_ptr<Node>(rhs);				\
      if (m == nullptr || n == nullptr)				\
	{							\
	  MsgStatus::error(#NAME " : missing arguments");	\
	}							\
    }								\
    virtual ~NAME() {}						\
    virtual void visit(AstVisitor& visitor) override;		\
  }								\

#define UNARY_RULE(NAME) struct NAME : public Node		\
  {explicit NAME (Node* node){name = #NAME;			\
      m = std::unique_ptr<Node>(node);				\
      n = nullptr;						\
      if (m == nullptr)						\
	{							\
	  MsgStatus::error(#NAME " : missing argument");	\
	}							\
    }								\
    virtual ~NAME() {}						\
    virtual void visit(AstVisitor& visitor) override;		\
  }								\

#define OPTIONAL_UNARY_RULE(NAME) struct NAME : public Node		\
  {explicit NAME (Node* node){name = #NAME;			\
      m = std::unique_ptr<Node>(node);				\
      n = nullptr;						\
    }								\
    virtual ~NAME() {}						\
    virtual void visit(AstVisitor& visitor) override;		\
  }								\

  TERNARY_RULE(VarDecl);
  
  BINARY_RULE(Seq);
  BINARY_RULE(Add);
  BINARY_RULE(Sub);
  BINARY_RULE(Mul);
  BINARY_RULE(Div);
  BINARY_RULE(Mod);

  BINARY_RULE(And);
  BINARY_RULE(Or);
  UNARY_RULE(Not);

  BINARY_RULE(Lt);
  BINARY_RULE(Le);
  BINARY_RULE(Leq);
  BINARY_RULE(Gt);
  BINARY_RULE(Ge);
  BINARY_RULE(Geq);
  BINARY_RULE(Equal);
  BINARY_RULE(Equiv);
  BINARY_RULE(Ne);
  BINARY_RULE(Neq);
  BINARY_RULE(Assign);
  BINARY_RULE(Loop);
  OPTIONAL_BINARY_RULE(IfEntry);

  OPTIONAL_UNARY_RULE(If);
  OPTIONAL_UNARY_RULE(Array);
  
  UNARY_RULE(Block);
  UNARY_RULE(Assert);
  UNARY_RULE(Dump);
  UNARY_RULE(Usub);

}
#endif // TREENODES
