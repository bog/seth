#include <iostream>
#include <TypeController.hpp>
#include <Lexer.hpp>
#include <Type.hpp>
#include <MsgStatus.hpp>
#include <Env.hpp>
#include <ValueOf.hpp>

namespace tree
{
  TypeController::TypeController(Env* env)
    : m_env { env }
    , m_err { false }
  {
  }

  bool TypeController::error()
  {
    return m_err;
  }

  /*virtual*/ void TypeController::accept(Array& node)
  {
    m_stack.push_back(Type::Array);
  }
  
  /*virtual*/ void TypeController::accept(Loop& node)
  {
    node.m->visit(*this);
    Type type_cond = m_stack.back();
    m_stack.pop_back();
    
    if (type_cond != Type::Bool)
      {
	MsgStatus::error("loop condition must be a bool");
      }

    auto stack = m_stack;
    node.n->visit(*this);
    stack.push_back(m_stack.back());
    m_stack = stack;
  }
  
  /*virtual*/ void TypeController::accept(If& node)
  {
    m_if_matched = false;
    node.m->visit(*this);

    if (!m_if_matched)
      {
	m_stack.push_back(Type::Bool);
      }
  }
  
  /*virtual*/ void TypeController::accept(IfEntry& node)
  {
    if (node.m)
      {	
	node.m->visit(*this);
	Type type = m_stack.back();
	m_stack.pop_back();

	if (type != Type::Bool)
	  {
	    MsgStatus::error("if conditions must be a bool");
	    m_err = true;
	  }
      }

    if (m_if_matched) { return; }    

    std::string res = "1";

    if(node.m)
      {
	ValueOf value { m_env };
	node.m->visit(value);
	res = value();
      }

    if (res == "1")
      {
	m_if_matched = true;
	node.n->visit(*this);
      }
  }
  
  /*virtual*/ void TypeController::accept(Assign& node)
  {    
    node.m->visit(*this);
    node.n->visit(*this);

    Type t1 = m_stack.back();
    m_stack.pop_back();

    Type t0 = m_stack.back();
    m_stack.pop_back();

    if (t0 != t1)
      {
	MsgStatus::error("type " + type_str(t0) + " expected");
	m_err = true;
      }

    m_stack.push_back(t0);
  }
  
  /*virtual*/ void TypeController::accept(Block& node)
  {
    m_env->block();
    node.m->visit(*this);
    m_env->unblock();
  }
  
  /*virtual*/ void TypeController::accept(Ident& node)
  {
    std::string name = node.value();

    if ( m_env->type(name) )
      {
	m_stack.push_back(*m_env->type(name));
      }
    else
      {
	MsgStatus::error("Cannot infer type of " + name);
	m_err = true;
	m_stack.push_back(Type::Invalid);
      }
  }
  
  /*virtual*/ void TypeController::accept(StringValue& node)
  {
  }
  
  /*virtual*/ void TypeController::accept(VarDecl& node) /*override*/
  {
    node.n->visit(*this);

    Type t0 = m_stack.back();
    m_stack.pop_back();
    
    std::string name = static_cast<tree::Ident*>(node.m.get())->value();
    
    if (node.type != t0)
      {
    	MsgStatus::error(name + " must have type " + type_str(t0) + "");
    	m_err = true;
      }
    else
      {
	m_env->declare(name, node.type, "0");
      }
  }

  /*virtual*/ void TypeController::accept(Seq& node) /*override*/
  {
    if (node.m)
      {
	node.m->visit(*this);
      }
	
    if (node.n)
      {	
	node.n->visit(*this);
      }   
  }
 
  /*virtual*/ void TypeController::accept(IntValue& node) /*override*/
  {
  }
  
  /*virtual*/ void TypeController::accept(Int& node) /*override*/
  {
    m_stack.push_back(Type::Int);
  }
  
  /*virtual*/ void TypeController::accept(BoolValue& node) /*override*/
  {
  }
  
  /*virtual*/ void TypeController::accept(Bool& node) /*override*/
  {
    m_stack.push_back(Type::Bool);
  }
  
  /*virtual*/ void TypeController::accept(Add& node) /*override*/
  {
    binary_type(node, Type::Int);
    m_stack.push_back(Type::Int);
  }
  
  /*virtual*/ void TypeController::accept(Sub& node) /*override*/
  {
    binary_type(node, Type::Int);
    m_stack.push_back(Type::Int);
  }
  
  /*virtual*/ void TypeController::accept(Mul& node) /*override*/
  {
    binary_type(node, Type::Int);
    m_stack.push_back(Type::Int);
  }
  
  /*virtual*/ void TypeController::accept(Div& node) /*override*/
  {
    binary_type(node, Type::Int);
    m_stack.push_back(Type::Int);
  }
  
  /*virtual*/ void TypeController::accept(Mod& node) /*override*/
  {
    binary_type(node, Type::Int);
    m_stack.push_back(Type::Int);
  }
  
  /*virtual*/ void TypeController::accept(And& node) /*override*/
  {
    binary_type(node, Type::Bool);
    m_stack.push_back(Type::Bool);
  }
  
  /*virtual*/ void TypeController::accept(Or& node) /*override*/
  {
    binary_type(node, Type::Bool);
    m_stack.push_back(Type::Bool);
  }
  
  /*virtual*/ void TypeController::accept(Not& node) /*override*/
  {
    unary_type(node, Type::Bool);
    m_stack.push_back(Type::Bool);
  }
  
  /*virtual*/ void TypeController::accept(Lt& node) /*override*/
  {
    binary_type(node, Type::Int);
    m_stack.push_back(Type::Bool);
  }
  
  /*virtual*/ void TypeController::accept(Le& node) /*override*/
  {
    binary_type(node, Type::Int);
    m_stack.push_back(Type::Bool);
  }
  
  /*virtual*/ void TypeController::accept(Leq& node) /*override*/
  {
    binary_type(node, Type::Int);
    m_stack.push_back(Type::Bool);
  }
  
  /*virtual*/ void TypeController::accept(Gt& node) /*override*/
  {
    binary_type(node, Type::Int);
    m_stack.push_back(Type::Bool);
  }
  
  /*virtual*/ void TypeController::accept(Ge& node) /*override*/
  {
    binary_type(node, Type::Int);
    m_stack.push_back(Type::Bool);
  }
  
  /*virtual*/ void TypeController::accept(Geq& node) /*override*/
  {
    binary_type(node, Type::Int);
    m_stack.push_back(Type::Bool);
  }
  
  /*virtual*/ void TypeController::accept(Equal& node) /*override*/
  {
    node.m->visit(*this);
    node.n->visit(*this);
    
    Type t1 = m_stack.back(); m_stack.pop_back();
    Type t0 = m_stack.back(); m_stack.pop_back();

    if (t0 != t1)
      {
	MsgStatus::error("Equal : type mismatch");
	return;
      }
    
    m_stack.push_back(Type::Bool);
  }
  
  /*virtual*/ void TypeController::accept(Equiv& node) /*override*/
  {
    node.m->visit(*this);
    node.n->visit(*this);
    
    Type t1 = m_stack.back(); m_stack.pop_back();
    Type t0 = m_stack.back(); m_stack.pop_back();

    if (t0 != t1)
      {
	MsgStatus::error("Equiv : type mismatch");
	return;
      }
    
    m_stack.push_back(Type::Bool);
  }
  
  /*virtual*/ void TypeController::accept(Ne& node) /*override*/
  {
    binary_type(node, Type::Int) || binary_type(node, Type::Bool);
    m_stack.push_back(Type::Bool);
  }
  
  /*virtual*/ void TypeController::accept(Neq& node) /*override*/
  {
    binary_type(node, Type::Int) || binary_type(node, Type::Bool);
    m_stack.push_back(Type::Bool);
  }
  
  /*virtual*/ void TypeController::accept(Assert& node) /*override*/
  {
    node.m->visit(*this);
    Type t = m_stack.back();
    m_stack.pop_back();

    if (t == Type::Int)
      {
	MsgStatus::error("Assert expect boolean");
	m_err = true;
      }
  }
  
  /*virtual*/ void TypeController::accept(Dump& node) /*override*/
  {
    node.m->visit(*this);
  }
  
  /*virtual*/ void TypeController::accept(Usub& node) /*override*/
  {
    unary_type(node, Type::Int);
    m_stack.push_back(Type::Int);
  }
  
  TypeController::~TypeController()
  {
  }

  bool TypeController::binary_type(Node& node, Type type)
  {
    node.m->visit(*this);
    node.n->visit(*this);
    
    Type t1 = m_stack.back();
    m_stack.pop_back();
    Type t0 = m_stack.back();
    m_stack.pop_back();
    
    if (t0 != type)
      {
    	m_err = true;
    	MsgStatus::error(node.name + " : " + "expecting " + type_str(type) + ", " + "got " + type_str(t0));
    	return false;
      }
    
    if (t1 != type)
      {
	m_err = true;
	MsgStatus::error(node.name + " : " + "expecting " + type_str(type) + ", " + "got " + type_str(t1));
	return false;
      }
    
    return true;
  }

  bool TypeController::unary_type(Node& node, Type type)
  {
    node.m->visit(*this);
    
    Type t0 = m_stack.back();
    m_stack.pop_back();

    if (t0 != type)
      {
	m_err = true;
	MsgStatus::error(node.name + " : " + "expecting " + type_str(type) + ", " + "got " + type_str(t0));
	return false;
      }

    return true;
  }
}
