#include <iostream>
#include <sstream>
#include <Lexer.hpp>
#include <MsgStatus.hpp>

Lexer::Lexer()
  : m_current { 0 }
  , m_tmp { "" }
{
}

Lexer::~Lexer()
{
}

std::vector<Token> Lexer::operator()(std::string const& source) const
{
  size_t line = 1;
  
  std::vector<Token> tokens;
  m_current = 0;
  m_tmp = "";
  
  while ( m_current < source.size() )
    {
      if ( match(source, " ") )
	{	  
	  // ignore whitespace
	}
      else if ( match(source, "\t") )
	{	  
	  // ignore tabs
	}
      else if ( match(source, "\n") )
	{
	  line++;
	}
      // RESERVED
      else if ( match(source, "loop") )
	{
	  Token token;
	  token.line = line;
	  token.type = TokenType::Loop;
	  token.inspect = "loop";
	  tokens.push_back(token);
	}
      else if ( match(source, "if") )
	{
	  Token token;
	  token.line = line;
	  token.type = TokenType::If;
	  token.inspect = "if";
	  tokens.push_back(token);
	}
      else if ( match(source, "else") )
	{
	  Token token;
	  token.line = line;
	  token.type = TokenType::Else;
	  token.inspect = "else";
	  tokens.push_back(token);
	}
      
      else if ( match(source, "int") )
	{
	  Token token;
	  token.line = line;
	  token.type = TokenType::Type;
	  token.inspect = "int";
	  tokens.push_back(token);
	}
      else if ( match(source, "bool") )
	{
	  Token token;
	  token.line = line;
	  token.type = TokenType::Type;
	  token.inspect = "bool";
	  tokens.push_back(token);
	}
      else if ( match(source, "array") )
	{
	  Token token;
	  token.line = line;
	  token.type = TokenType::Type;
	  token.inspect = "array";
	  tokens.push_back(token);
	}
      // OTHERS
      else if ( match(source, ",") )
	{
	  Token token;
	  token.line = line;
	  token.type = TokenType::Comma;
	  token.inspect = ",";
	  tokens.push_back(token);
	}
      else if ( match(source, "[") )
	{
	  Token token;
	  token.line = line;
	  token.type = TokenType::OpenArray;
	  token.inspect = "[";
	  tokens.push_back(token);
	}
      else if ( match(source, "]") )
	{
	  Token token;
	  token.line = line;
	  token.type = TokenType::CloseArray;
	  token.inspect = "]";
	  tokens.push_back(token);
	}
      else if ( match(source, "{") )
	{
	  Token token;
	  token.line = line;
	  token.type = TokenType::OpenBlock;
	  token.inspect = "{";
	  tokens.push_back(token);
	}
      else if ( match(source, "}") )
	{
	  Token token;
	  token.line = line;
	  token.type = TokenType::CloseBlock;
	  token.inspect = "}";
	  tokens.push_back(token);
	}
      else if ( match(source, ":") )
	{
	  Token token;
	  token.line = line;
	  token.type = TokenType::Colon;
	  token.inspect = ":";
	  tokens.push_back(token);
	}
      else if ( match(source, ";") )
	{
	  Token token;
	  token.line = line;
	  token.type = TokenType::SemiColon;
	  token.inspect = ";";
	  tokens.push_back(token);
	}
      else if ( match(source, "_dump") )
	{
	  Token token;
	  token.line = line;
	  token.type = TokenType::Dump;
	  token.inspect = "_dump";
	  tokens.push_back(token);
	}

      else if ( match(source, "_assert") )
	{
	  Token token;
	  token.line = line;
	  token.type = TokenType::Assert;
	  token.inspect = "_assert";
	  tokens.push_back(token);
	}

      // Unary token
      // Not, True, False, OpenPar, ClosePar
  
      else if ( match(source, "true") )
	{
	  Token token;
	  token.line = line;
	  token.type = TokenType::True;
	  token.inspect = "true";
	  token.value.b = true;
	  tokens.push_back(token);
	}

      else if ( match(source, "false") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = "false";
	  token.type = TokenType::False;
	  token.value.b =  false;
	  tokens.push_back(token);
	}

      else if ( match(source, "(") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = "(";
	  token.type = TokenType::OpenPar;      
	  tokens.push_back(token);
	}

      else if ( match(source, ")") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = ")";
	  token.type = TokenType::ClosePar;      
	  tokens.push_back(token);
	}
      
      // Int, And, Or
      else if ( match_int(source) )
	{
	  Token token;
	  token.line = line;
	  token.inspect = m_tmp.c_str();
	  token.type = TokenType::Int;
	  token.value.i32 = std::atoi( m_tmp.c_str() );

	  tokens.push_back(token);
	}

      else if ( match(source, "&&") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = "&&";
	  token.type = TokenType::And;
	  tokens.push_back(token);
	}

      else if ( match(source, "||") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = "||";
	  token.type = TokenType::Or;
	  tokens.push_back(token);
	}

      // Add, Mul, Sub, Div, Mod
      else if ( match(source, "+") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = "+";
	  token.type = TokenType::Add;
	  tokens.push_back(token);
	}

      else if ( match(source, "-") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = "-";
	  token.type = TokenType::Sub;
	  tokens.push_back(token);
	}

      else if ( match(source, "*") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = "*";
	  token.type = TokenType::Mul;
	  tokens.push_back(token);
	}
      else if ( match(source, "//") )
      	{
      	  skip_line(source);
      	}
      else if ( match(source, "/") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = "/";
	  token.type = TokenType::Div;
	  tokens.push_back(token);
	}

      else if ( match(source, "%") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = "%";
	  token.type = TokenType::Mod;
	  tokens.push_back(token);
	}
      
      // Comparisons Lt, Gt, Le, Ge, Leq, Geq, Equal, Equiv, Ne, Neq

      else if ( match(source, "<==") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = "<==";
	  token.type = TokenType::Leq;
	  tokens.push_back(token);	  
	}
      else if ( match(source, "<=") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = "<=";
	  token.type = TokenType::Le;
	  tokens.push_back(token);
	}
      else if ( match(source, "<") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = "<";
	  token.type = TokenType::Lt;
	  tokens.push_back(token);
	}

      else if ( match(source, ">==") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = ">==";
	  token.type = TokenType::Geq;
	  tokens.push_back(token);
	}
      else if ( match(source, ">=") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = ">=";
	  token.type = TokenType::Ge;
	  tokens.push_back(token);
	}
      else if ( match(source, ">") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = ">";
	  token.type = TokenType::Gt;
	  tokens.push_back(token);
	}

      else if ( match(source, "===") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = "===";
	  token.type = TokenType::Equiv;
	  tokens.push_back(token);
	}
      else if ( match(source, "==") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = "==";
	  token.type = TokenType::Equal;
	  tokens.push_back(token);
	}
      else if ( match(source, "=") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = "=";
	  token.type = TokenType::Assign;
	  tokens.push_back(token);
	}
      else if ( match(source, "!==") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = "!==";
	  token.type = TokenType::Neq;
	  tokens.push_back(token);
	}
      else if ( match(source, "!=") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = "!=";
	  token.type = TokenType::Ne;
	  tokens.push_back(token);
	}
      else if ( match(source, "!") )
	{
	  Token token;
	  token.line = line;
	  token.inspect = "!";
	  token.type = TokenType::Not;      
	  tokens.push_back(token);
	}
      // Ident
      else if ( source[m_current] >= 'a' && source[m_current] <= 'z' )
	{
	  std::string ident = "";

	  auto const match_ident = [](char c) {
				     return c >= 'a' && c <= 'z';
				   };
	  
	  while ( match_ident(source[m_current]) )
	    {
	      ident += source[m_current];
	      m_current++;
	    }	  

	  Token token;
	  token.line = line;
	  token.inspect = ident;
	  token.type = TokenType::Ident;      
	  tokens.push_back(token);
	}
      else
	{
	  if (m_current < source.size())
	    {
	      std::stringstream ss;
	      ss << "line " << line << " : " << "unknown token \"" << source[m_current] << "\"";
	      MsgStatus::error(ss.str());
	    }
	  
	  m_current++;
	}
    }
  
  return tokens;
}

bool Lexer::match(std::string const& source, std::string const& values) const
{
  for (size_t i=0; i<values.size(); i++)
    {
      if ( source[m_current + i] != values[i] )
	{
	  return false;
	}
    }
  
  m_current += values.size();
      
  return true;
}

bool Lexer::match_int(std::string const& source) const
{
  m_tmp = "";
  size_t offset = 0;
  
  while ( isdigit(source[m_current + offset]) )
    {
      m_tmp += source[m_current + offset];
      offset++;
    }

  if ( offset > 0 )
    {
      m_current += offset;
      return true;
    }
  
  return false;
}

void Lexer::skip_line(std::string const& source) const
{
  while ( m_current < source.size() && source[m_current] != '\n' )
    {
      m_current++;
    }
}
