#ifndef LEXER_HPP
#define LEXER_HPP

#include <std.pch.hpp>

enum class TokenType {Not, And, Or, Add, Mul, Sub, Div, Mod, Int, True, False, OpenPar, ClosePar, Dump, Assert,
		      Lt, Gt, Le, Ge, Leq, Geq, Equal, Equiv, Ne, Neq, SemiColon, Colon, Assign,
		      Ident, Type, OpenBlock, CloseBlock, If, Else, Loop, OpenArray, CloseArray, Comma};

struct Token
{
  TokenType type;
  std::string inspect = "";
  size_t line;
  union
  {
    int i32;
    bool b;
  } value;
};

class Lexer
{
public:
  Lexer();    
  virtual ~Lexer();
  
  std::vector<Token> operator()(std::string const& source) const;
  
private:
  mutable size_t m_current;
  mutable std::string m_tmp;
  
  bool match(std::string const& source, std::string const& values) const;
  bool match_int(std::string const& source) const;
  void skip_line(std::string const& source) const;
  
  Lexer(Lexer const& lexer) = delete;
  Lexer& operator=(Lexer const& lexer) = delete;
};

#endif // LEXER
