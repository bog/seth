#include <iostream>
#include <Env.hpp>
#include <Type.hpp>
#include <MsgStatus.hpp>
#include <TreeNodes.hpp>
#include <MultiTypeArray.hpp>

size_t Env::ArrayAddr = 0;

/*friend*/ std::ostream& operator<<(std::ostream& os, Env const& env)
{
  os << *env.m_map;
  return os;
}

/*friend*/ std::ostream& operator<<(std::ostream& os, EnvMap const& envmap)
{
  os << "---------------- env " << &envmap << std::endl;
  
  for (auto const p : envmap.map)
    {
      os << "-> " << p.first << " : " << type_str(p.second.first) << " = " << p.second.second << std::endl;
    }

  if (envmap.prev)
    {
      os << *(envmap.prev);
    }
  
  return os;
}

Env::Env()
  : m_map { new EnvMap }
{  
}

void Env::declare(std::string const& name, Type type, std::string const& value)
{
  if ( local_contains(name) )
    {
      MsgStatus::error("Variable " + name + " already exists.");
      return;
    }
  
  m_map->map.insert({name, {type, value}});
}

size_t Env::declare_array(tree::Array const& array)
{  
  return Env::ArrayAddr++;
}


bool Env::set(std::string const& name, std::string const& value)
{
  return m_map->set(name, value);
}

std::optional<std::string> Env::get(std::string const& name) const
{
  return m_map->get(name);
}

std::optional<Type> Env::type(std::string const& name) const
{
  return m_map->type(name);
}


bool Env::contains(std::string const& name) const
{
  return m_map->contains(name);
}

bool Env::local_contains(std::string const& name) const
{
  return m_map->local_contains(name);
}

void Env::block()
{
  EnvMap* new_map = new EnvMap;
  new_map->prev = m_map;

  m_map = new_map;
}

void Env::unblock()
{
  if (m_map->prev)
    {
      EnvMap* prev = m_map->prev;
      delete m_map;
      m_map = prev;
    }
}

Env::~Env()
{
  if (m_map)
    {
      if (m_map->prev)
	{
	  delete m_map->prev;
	}
      
      delete m_map;
      m_map = nullptr;
    }
}

bool EnvMap::set(std::string const& name, std::string const& value)
{
  auto itr = map.find(name);

  if (itr == map.end())
    {
      if (prev)
	{
	  return prev->set(name, value);
	}
      else
	{
	  return false;
	}
    }
  else
    {
      map[name] = {map[name].first, value};
      return true;
    }
}

std::optional<std::string> EnvMap::get(std::string const& name) const
{
  auto itr = map.find(name);
  
  if ( itr == map.end() )
    {
      if (prev)
	{
	  return prev->get(name);
	}
      
      MsgStatus::error("variable " + name + " not declared.");
      return std::nullopt;
    }
  
  return itr->second.second;
}

std::optional<Type> EnvMap::type(std::string const& name) const
{
  auto itr = map.find(name);
  
  if ( itr == map.end() )
    {
      if (prev)
	{
	  return prev->type(name);
	}
      else
	{
	  return std::nullopt;
	}
    }
  
  return itr->second.first;
}

bool EnvMap::contains(std::string const& name) const
{
  auto itr = map.find(name);
  
  if ( itr == map.end() )
    {
      if (prev)
	{
	  return prev->contains(name);
	}
      
      return false;
    }

  return true;

}

bool EnvMap::local_contains(std::string const& name) const
{
  auto itr = map.find(name);
  
  return (itr != map.end());
}

  
/*virtual*/ EnvMap::~EnvMap()
{
  
}
