#include <iostream>
#include <ThreeAddressCode.hpp>
#include <Env.hpp>
#include <ValueOf.hpp>

namespace tree
{
  tac_t::tac_t(OpCode o, std::optional<std::string> const& s0, std::optional<std::string> const& s1)
    : op { o }
    , lhs { s0 }
    , rhs { s1 }
  {
  }

  std::string opcode_str(OpCode op)
  {
    std::vector<std::string> v = {
				  "Push",
				  "Add",
				  "Sub",
				  "Mul",
				  "Div",
				  "Mod",
				  "Usub",
				  "And",
				  "Or",
				  "Not",
				  "Lt",
				  "Le",
				  "Leq",
				  "Gt",
				  "Ge",
				  "Geq",
				  "Equal",
				  "Equiv",
				  "Ne",
				  "Neq",
				  "Assert",
				  "Dump",
				  "Block"
    };
    
    return v[static_cast<int>(op)];
  }

  bool tac_t::operator==(tac_t const& t) const
  {
    return true;
  }

  ThreeAddressCode::ThreeAddressCode(Env* env)
    : m_env { env }
  {
  }

  std::vector<tac_t> ThreeAddressCode::instructions()
  {
    return m_instructions;
  }

  /*virtual*/ void ThreeAddressCode::accept(Array& node)
  {
  }
  
  /*virtual*/ void ThreeAddressCode::accept(Loop& node)
  {
    while (true)
      {
	ValueOf value { m_env };
	node.m->visit(value);
    
	if (value() == "1")
	  {
	    node.n->visit(*this);
	  }
	else
	  {
	    break;
	  }
      }
  }
  
  /*virtual*/ void ThreeAddressCode::accept(If& node)
  {
    m_if_matched = false;
    node.m->visit(*this);

    if (!m_if_matched)
      {
	m_instructions.push_back(tac_t {OpCode::Push, "0" , std::nullopt});
      }
  }

  /*virtual*/ void ThreeAddressCode::accept(IfEntry& node)
  {
    if (m_if_matched) { return; }
    
    ValueOf value { m_env };
    
    if (node.m)
      {
	node.m->visit(value);

	if ("1" == value())
	  {
	    m_if_matched = true;
	    node.n->visit(*this);
	  }
      }
    else
      {
	m_if_matched = true;
	node.n->visit(*this);
      }
  }
  
  /*virtual*/ void ThreeAddressCode::accept(Assign& node)
  {
    node.n->visit(*this);
    std::string name = static_cast<tree::Ident*>(node.m.get())->value();
    ValueOf vo { m_env };
    node.n->visit(vo);
    std::string value = vo();
    
    m_env->set(name, value);
  }
  
  /*virtual*/ void ThreeAddressCode::accept(Block& node)
  {
    m_env->block();    
    node.m->visit(*this);
    m_env->unblock();
  }
  
  /*virtual*/ void ThreeAddressCode::accept(Ident& node)
  {
    std::optional<std::string> value;
    value = m_env->get(static_cast<StringValue*>(node.m.get())->value);

    if (value)
      {
	m_instructions.push_back(tac_t {OpCode::Push, *value, std::nullopt});
      }
    else
      {
	m_instructions.push_back(tac_t {OpCode::Push, "0", std::nullopt});
      }
  }
  
  /*virtual*/ void ThreeAddressCode::accept(StringValue& node)
  {

  }

  /*virtual*/ void ThreeAddressCode::accept(Seq& node)
  {
    if (node.m)
      {
	node.m->visit(*this);
      }
    
    if (node.n)
      {
	node.n->visit(*this);
      }
  }

  /*virtual*/ void ThreeAddressCode::accept(VarDecl& node)
  {
    std::string name;
    Type type = node.type;
    std::string value;

    name = static_cast<tree::Ident*>(node.m.get())->value();

    if (type == Type::Array)
      {
	value = std::to_string(m_env->declare_array(*static_cast<tree::Array*>(node.n.get())));
		
	m_env->declare(name, type, value);
      }
    else
      {
	tree::ValueOf value_of { m_env };
	node.n->visit(value_of);
	value = value_of();
	m_env->declare(name, type, value);
      }
  }
  
  /*virtual*/ void ThreeAddressCode::accept(IntValue& node)
  {
    m_instructions.push_back(tac_t {OpCode::Push, std::to_string(node.value), std::nullopt});
  }
  
  /*virtual*/ void ThreeAddressCode::accept(Int& node)
  {
    node.m->visit(*this);    
  }
  
  /*virtual*/ void ThreeAddressCode::accept(BoolValue& node)
  {
    m_instructions.push_back(tac_t {OpCode::Push, (node.value ? "1" : "0") , std::nullopt});
  }
  
  /*virtual*/ void ThreeAddressCode::accept(Bool& node)
  {
    node.m->visit(*this);
  }
  
  /*virtual*/ void ThreeAddressCode::accept(Add& node)
  {
    node.n->visit(*this);
    node.m->visit(*this);

    m_instructions.push_back(tac_t {OpCode::Add, std::nullopt , std::nullopt});
  }
  
  /*virtual*/ void ThreeAddressCode::accept(Sub& node)
  {
    node.n->visit(*this);
    node.m->visit(*this);

    m_instructions.push_back(tac_t {OpCode::Sub, std::nullopt , std::nullopt});
  }
  
  /*virtual*/ void ThreeAddressCode::accept(Mul& node)
  {
    node.n->visit(*this);
    node.m->visit(*this);

    m_instructions.push_back(tac_t {OpCode::Mul, std::nullopt , std::nullopt});
  }
  
  /*virtual*/ void ThreeAddressCode::accept(Div& node)
  {
    node.n->visit(*this);
    node.m->visit(*this);

    m_instructions.push_back(tac_t {OpCode::Div, std::nullopt , std::nullopt});
  }
  
  /*virtual*/ void ThreeAddressCode::accept(Mod& node)
  {
    node.n->visit(*this);
    node.m->visit(*this);

    m_instructions.push_back(tac_t {OpCode::Mod, std::nullopt , std::nullopt});
  }
  
  /*virtual*/ void ThreeAddressCode::accept(And& node)
  {
    node.n->visit(*this);
    node.m->visit(*this);

    m_instructions.push_back(tac_t {OpCode::And, std::nullopt , std::nullopt});
  }
  
  /*virtual*/ void ThreeAddressCode::accept(Or& node)
  {
    node.n->visit(*this);
    node.m->visit(*this);

    m_instructions.push_back(tac_t {OpCode::Or, std::nullopt , std::nullopt});

  }
  
  /*virtual*/ void ThreeAddressCode::accept(Not& node)
  {
    node.m->visit(*this);

    m_instructions.push_back(tac_t {OpCode::Not, std::nullopt , std::nullopt});

  }
  
  /*virtual*/ void ThreeAddressCode::accept(Lt& node)
  {
    node.n->visit(*this);
    node.m->visit(*this);

    m_instructions.push_back(tac_t {OpCode::Lt, std::nullopt , std::nullopt});

  }
  
  /*virtual*/ void ThreeAddressCode::accept(Le& node)
  {
    node.n->visit(*this);
    node.m->visit(*this);

    m_instructions.push_back(tac_t {OpCode::Le, std::nullopt , std::nullopt});

  }
  
  /*virtual*/ void ThreeAddressCode::accept(Leq& node)
  {
    node.n->visit(*this);
    node.m->visit(*this);

    m_instructions.push_back(tac_t {OpCode::Leq, std::nullopt , std::nullopt});

  }
  
  /*virtual*/ void ThreeAddressCode::accept(Gt& node)
  {
    node.n->visit(*this);
    node.m->visit(*this);

    m_instructions.push_back(tac_t {OpCode::Gt, std::nullopt , std::nullopt});

  }
  
  /*virtual*/ void ThreeAddressCode::accept(Ge& node)
  {
    node.n->visit(*this);
    node.m->visit(*this);

    m_instructions.push_back(tac_t {OpCode::Ge, std::nullopt , std::nullopt});

  }
  
  /*virtual*/ void ThreeAddressCode::accept(Geq& node)
  {
    node.n->visit(*this);
    node.m->visit(*this);

    m_instructions.push_back(tac_t {OpCode::Geq, std::nullopt , std::nullopt});

  }
  
  /*virtual*/ void ThreeAddressCode::accept(Equal& node)
  {
    node.n->visit(*this);
    node.m->visit(*this);

    m_instructions.push_back(tac_t {OpCode::Equal, std::nullopt , std::nullopt});

  }
  
  /*virtual*/ void ThreeAddressCode::accept(Equiv& node)
  {
    node.n->visit(*this);
    node.m->visit(*this);

    m_instructions.push_back(tac_t {OpCode::Equiv, std::nullopt , std::nullopt});
    
  }
  
  /*virtual*/ void ThreeAddressCode::accept(Ne& node)
  {
    node.n->visit(*this);
    node.m->visit(*this);

    m_instructions.push_back(tac_t {OpCode::Ne, std::nullopt , std::nullopt});

  }
  
  /*virtual*/ void ThreeAddressCode::accept(Neq& node)
  {
    node.n->visit(*this);
    node.m->visit(*this);

    m_instructions.push_back(tac_t {OpCode::Neq, std::nullopt , std::nullopt});
  }
  
  /*virtual*/ void ThreeAddressCode::accept(Assert& node)
  {
    node.m->visit(*this);

    m_instructions.push_back(tac_t {OpCode::Assert, std::nullopt , std::nullopt});
  }
  
  /*virtual*/ void ThreeAddressCode::accept(Dump& node)
  {
    node.m->visit(*this);

    m_instructions.push_back(tac_t {OpCode::Dump, std::nullopt , std::nullopt});
  }
  
  /*virtual*/ void ThreeAddressCode::accept(Usub& node)
  {
    node.m->visit(*this);

    m_instructions.push_back(tac_t {OpCode::Usub, std::nullopt , std::nullopt});
  }
  
  
  ThreeAddressCode::~ThreeAddressCode()
  {
  }
}
