#include <Type.hpp>

std::string type_str(Type t)
{
  switch (t)
    {
      case Type::Invalid: return "invalid";
      case Type::Int: return "int";
      case Type::Any: return "any";
      case Type::Bool: return "bool";
      case Type::Array: return "array";
    }

  return "";
}
