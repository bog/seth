#ifndef TYPE_HPP
#define TYPE_HPP
#include <std.pch.hpp>

enum class Type {
		 Invalid,
		 Any,
		 Int,
		 Bool,
		 Array
};

std::string type_str(Type t);

#endif
