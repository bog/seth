#include <iostream>
#include <fstream>
#include <vector>
#include <Lexer.hpp>
#include <Parser.hpp>
#include <TreeNodes.hpp>
#include <MsgStatus.hpp>
#include <TypeController.hpp>
#include <ThreeAddressCode.hpp>
#include <Ccompiler.hpp>
#include <Env.hpp>

int main(int argc, char** argv)
{
  std::string input_path;
  std::string output_path;
  
  if (argc == 3)
    {
      input_path = argv[1];
      output_path = argv[2];
    }
  else
    {
      std::cerr<< "Usage : " <<std::endl;
      std::cerr<< argv[0] << " <source.7l> <executable>" <<std::endl;
      exit(-1);
    }

  std::ifstream input_file { input_path };

  if (!input_file)
    {
      std::cerr<< "Cannot open \""<<input_path<<"\"" <<std::endl;
      exit(-1);
    }

  std::string source;

  {
    std::string line;

    while ( std::getline(input_file, line) )
      {
	source += line;

	if (!input_file.eof())
	  {
	    source += "\n";
	  }
      }
  }
  
  MsgStatus::init();


  Lexer l;
  Parser p {l};
  tree::Node const& n = p(source);

  Env env_tac;
  tree::ThreeAddressCode tac {&env_tac};
  
  Env env_tc;
  tree::TypeController tc { &env_tc };

  const_cast<tree::Node&>(n).visit(tac);
  const_cast<tree::Node&>(n).visit(tc);
  
  Ccompiler compiler { tac };
  
  std::vector<std::string> errs = MsgStatus::errors_str();

  if ( errs.empty() )
    {
      std::ofstream output { output_path + ".c" };

      if (!output)
	{
	  std::cerr<< "Cannot create output." <<std::endl;
	  exit(-1);
	}

      output << compiler.header() <<std::endl;
      output << compiler.source() <<std::endl;
      output << compiler.footer() <<std::endl;

      std::string cfile = output_path + ".c";
      std::string compile = "/usr/bin/gcc " + cfile + " -g -o " + output_path;
      std::string move = "/usr/bin/mv " + cfile + " /tmp";
      
      system(compile.c_str());
      system(move.c_str());
    }
  else
    {
      std::cerr<< "Error" <<std::endl;

      for (auto const& err : errs)
	{
	  std::cerr<< "-> " << err <<std::endl;
	}

      exit(-1);
    }
  
  
  return 0;
}
