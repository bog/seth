#ifndef MSGSTATUS_HPP
#define MSGSTATUS_HPP
#include <std.pch.hpp>

class MsgStatus
{
public:
  
  static void init();
  static std::vector<std::string> errors_str();
  static void error(std::string const& msg);
  
  MsgStatus() {};  
  virtual ~MsgStatus() {};

private:
  static std::vector<std::string> m_errors;
  
  MsgStatus(MsgStatus const& msgstatus) = delete;
  MsgStatus& operator=(MsgStatus const& msgstatus) = delete;
};

#endif // MSGSTATUS
