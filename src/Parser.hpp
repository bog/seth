#ifndef PARSER_HPP
#define PARSER_HPP
#include <std.pch.hpp>

namespace tree { class Node; }

class Lexer;
class Token;
enum class TokenType;
class Parser
{
public:
  explicit Parser(Lexer const& lexer);    
  virtual ~Parser();

  tree::Node const& operator()(std::string const& input) const;  
private:
  Lexer const& m_lexer;
  mutable size_t m_current;
  mutable tree::Node* m_result;

  bool match(std::vector<Token> const& tokens, std::vector<TokenType> const& toktypes) const;
  bool match(std::vector<Token> const& tokens, TokenType const& toktype) const;
  bool at_end(std::vector<Token> const& tokens) const;
  tree::Node* instr(std::vector<Token> const& tokens) const;
  tree::Node* expr(std::vector<Token> const& tokens) const;
  tree::Node* ifentry(std::vector<Token> const& tokens) const;
  tree::Node* cmp(std::vector<Token> const& tokens) const;
  tree::Node* andor(std::vector<Token> const& tokens) const;
  tree::Node* addsub(std::vector<Token> const& tokens) const;
  tree::Node* mod(std::vector<Token> const& tokens) const;
  tree::Node* muldiv(std::vector<Token> const& tokens) const;
  tree::Node* unary(std::vector<Token> const& tokens) const;
  tree::Node* literal(std::vector<Token> const& tokens) const;
  
  Parser(Parser const& parser) = delete;
  Parser& operator=(Parser const& parser) = delete;
};

#endif // PARSER
