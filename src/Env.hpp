#ifndef ENV_HPP
#define ENV_HPP
#include <std.pch.hpp>

enum class Type;
class MultiTypeArray;

namespace tree { class Array; }

struct EnvMap
{
  friend std::ostream& operator<<(std::ostream& os, EnvMap const& envmap);
  std::unordered_map<std::string, std::pair<Type, std::string>> map;
  EnvMap* prev = nullptr;

  bool set(std::string const& name, std::string const& value);
  std::optional<std::string> get(std::string const& name) const;
  std::optional<Type> type(std::string const& name) const;
  bool contains(std::string const& name) const;
  bool local_contains(std::string const& name) const;

  virtual ~EnvMap();
};

class Env
{
public:
  friend std::ostream& operator<<(std::ostream& os, Env const& env);
  Env();
  void declare(std::string const& name, Type type, std::string const& value);
  size_t declare_array(tree::Array const& array);
  std::optional<std::string> get(std::string const& name) const;
  bool set(std::string const& name, std::string const& value);
  std::optional<Type> type(std::string const& name) const;
  bool contains(std::string const& name) const;
  bool local_contains(std::string const& name) const;
  void block();
  void unblock();
  
  virtual ~Env();

private:
  EnvMap* m_map;
  static size_t ArrayAddr;
  std::vector<std::unique_ptr<MultiTypeArray>> m_arrays;
  Env(Env const& env) = delete;
  Env& operator=(Env const& env) = delete;
};

#endif // ENV
