#ifndef MULTITYPEARRAY_HPP
#define MULTITYPEARRAY_HPP
#include <std.pch.hpp>

enum class Type;
class MultiTypeArray;

struct ArrayEntry
{ 
  Type type;
  
  union
  {
    int i32;
    bool b;
  } value;

  MultiTypeArray* array;
};

class MultiTypeArray
{
public:
  MultiTypeArray();
  ArrayEntry get(size_t index);
  void push_back(int n);
  void push_back(bool b);
  void push_back(MultiTypeArray& array);
  virtual ~MultiTypeArray();

private:
  std::vector<ArrayEntry> m_entries;
  MultiTypeArray(MultiTypeArray const& multitypearray) = delete;
  MultiTypeArray& operator=(MultiTypeArray const& multitypearray) = delete;
};

#endif // MULTITYPEARRAY
