#ifndef THREEADDRESSCODE_HPP
#define THREEADDRESSCODE_HPP
#include <std.pch.hpp>
#include <AstVisitor.hpp>

class Env;

namespace tree
{
  enum class OpCode
    {
     Push,
     Add,
     Sub,
     Mul,
     Div,
     Mod,
     Usub,
     And,
     Or,
     Not,
     Lt,
     Le,
     Leq,
     Gt,
     Ge,
     Geq,
     Equal,
     Equiv,
     Ne,
     Neq,
     Assert,
     Dump,
     Block
    };

  std::string opcode_str(OpCode op);
  
  struct tac_t
  {
    tac_t(OpCode o, std::optional<std::string> const& s0, std::optional<std::string> const& s1);

    OpCode op;
    std::optional<std::string> lhs;
    std::optional<std::string> rhs;

    bool operator==(tac_t const& t) const;
  };

  class ThreeAddressCode : public AstVisitor
  {
  public:
    ThreeAddressCode(Env* env);
    std::vector<tac_t> instructions();
    virtual void accept(Array& node) override;
    virtual void accept(Loop& node) override;
    virtual void accept(If& node) override;
    virtual void accept(IfEntry& node) override;
    virtual void accept(Assign& node) override;
    virtual void accept(Block& node) override;
    virtual void accept(Ident& node) override;
    virtual void accept(StringValue& node) override;
    virtual void accept(VarDecl& node) override;
    virtual void accept(Seq& node) override;
    virtual void accept(IntValue& node) override;
    virtual void accept(Int& node) override;
    virtual void accept(BoolValue& node) override;
    virtual void accept(Bool& node) override;
    virtual void accept(Add& node) override;
    virtual void accept(Sub& node) override;
    virtual void accept(Mul& node) override;
    virtual void accept(Div& node) override;
    virtual void accept(Mod& node) override;
    virtual void accept(And& node) override;
    virtual void accept(Or& node) override;
    virtual void accept(Not& node) override;
    virtual void accept(Lt& node) override;
    virtual void accept(Le& node) override;
    virtual void accept(Leq& node) override;
    virtual void accept(Gt& node) override;
    virtual void accept(Ge& node) override;
    virtual void accept(Geq& node) override;
    virtual void accept(Equal& node) override;
    virtual void accept(Equiv& node) override;
    virtual void accept(Ne& node) override;
    virtual void accept(Neq& node) override;
    virtual void accept(Assert& node) override;
    virtual void accept(Dump& node) override;
    virtual void accept(Usub& node) override;

    virtual ~ThreeAddressCode();

  private:
    Env* m_env;
    std::vector<tac_t> m_instructions;
    bool m_if_matched;
    ThreeAddressCode(ThreeAddressCode const& threeaddresscode) = delete;
    ThreeAddressCode& operator=(ThreeAddressCode const& threeaddresscode) = delete;
  };
}
#endif // THREEADDRESSCODE
