#include <MultiTypeArray.hpp>
#include <Type.hpp>

MultiTypeArray::MultiTypeArray()
{
}

ArrayEntry MultiTypeArray::get(size_t index)
{
  return m_entries[index];
}

void MultiTypeArray::push_back(int n)
{
  ArrayEntry entry;
  entry.type = Type::Int;
  entry.value.i32 = n;
  m_entries.push_back(entry);
}

void MultiTypeArray::push_back(bool b)
{
  ArrayEntry entry;
  entry.type = Type::Bool;
  entry.value.b = b;
  m_entries.push_back(entry);
}

void MultiTypeArray::push_back(MultiTypeArray& array)
{
  ArrayEntry entry;
  entry.type = Type::Array;
  entry.array = &array;
  m_entries.push_back(entry);
}

MultiTypeArray::~MultiTypeArray()
{
}
